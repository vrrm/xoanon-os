;;;; package.lisp

(defpackage #:xoanon
  (:use
   #:cl
   ))

(defpackage #:xoanon-utils
  (:use
   :cl
   :osicat
   :flexi-streams
   :cxml
   :inferior-shell
   :bordeaux-threads
   :cl-fad))
