(in-package #:xoanon)

;; (defun operate-on-params (params)
;;   (multiple-value-bind
;;         (val string-length)
;;       (parse-integer (car params) :radix (cadr params))
;;     (list val
;;           (coerce
;;            (ceiling (log (expt (cadr params) string-length) 2))
;;            'integer))))

;; (defun parse-string-as-int-field (number-string &optional (radix 8))
;;   (let* ((implied-hex-radix
;;           (multiple-value-list
;;            (cl-ppcre:scan-to-strings "^(0[xX])([0-9a-fA-F]+)$" number-string)))
;;          (implied-oct-radix
;;           (multiple-value-list
;;            (cl-ppcre:scan-to-strings "^(0)([0-7]+)$" number-string)))
;;          (params
;;           (cond ((cadr implied-hex-radix)
;;                  (list (aref (cadr implied-hex-radix) 1) 16))
;;                 ((cadr implied-oct-radix)
;;                  (list (aref (cadr implied-oct-radix) 1) 8))
;;                 ((list number-string radix)))))
;;     (operate-on-params params)))

;; ;; If bit-field is in c string form, convert it to a lisp bitfield.
;; (defun convert-bit-field (bit-field)
;;   (cond ((stringp bit-field)
;;          (parse-string-as-int-field bit-field))
;;         (bit-field)))

;; ;; bit 'and' useful for porting c
;; ;;
;; ;; if correspending bits of the to two parameters are the same, then
;; ;; the result bit array will be set on for that bit. Otherwise it's
;; ;; set to 0.  The two representations of a bit field are '(<integer>
;; ;; <width>) or <octal-text>.
;; (defun & (bit-field1 bit-field2)
;;   (let ((normalized-bit-field1 (convert-bit-field bit-field1))
;;         (normalized-bit-field2 (convert-bit-field bit-field2)))
;;     (cond ((eql (cadr normalized-bit-field1)
;;                 (cadr normalized-bit-field2))
;;            (let ((field-width (cadr normalized-bit-field2)))
;;              (logand
;;               (ldb (byte field-width 0) (car normalized-bit-field1))
;;               (ldb (byte field-width 0) (car normalized-bit-field2)))))
;;           ((error "Field widths must be the same.")))))
