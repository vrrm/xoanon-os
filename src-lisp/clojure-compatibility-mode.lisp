;; from: 
;; public static Object  Compiler::load(Reader rdr, String sourcePath, String sourceName)
(defun load (reader, source-path, source-name) 
  ;; Note, in this case eof == ret
  (let ((eof  '())
        (ret  null)
        (pushback-reader =
                         (get line-numbering-pushback-reader))
    ;;; for our purposes, no thread bindings.
    ;;Var.pushThreadBindings(
        (RT (list ('LOADER make-class-loader)
                  ('SOURCE_PATH source-path)
                  ('SOURCE source-name)
                  ('METHOD nil)
                  ('LOCAL-ENV null)
                  ('LOOP-LOCALS, null)
                  ('NEXT-LOCAL-NUM, 0)
			       RT.CURRENT_NS, RT.CURRENT_NS.deref(),
			       LINE_BEFORE, pushbackReader.getLineNumber(),
			       LINE_AFTER, pushbackReader.getLineNumber()
			       ,RT.UNCHECKED_MATH, RT.UNCHECKED_MATH.deref()
					,RT.WARN_ON_REFLECTION, RT.WARN_ON_REFLECTION.deref()
			       ,RT.DATA_READERS, RT.DATA_READERS.deref()
                ;; )
                )

	try
		{
		for(Object r = LispReader.read(pushbackReader, false, EOF, false); r != EOF;
		    r = LispReader.read(pushbackReader, false, EOF, false))
			{
			LINE_AFTER.set(pushbackReader.getLineNumber());
			ret = eval(r,false);
			LINE_BEFORE.set(pushbackReader.getLineNumber());
			}
		}
	catch(LispReader.ReaderException e)
		{
		throw new CompilerException(sourcePath, e.line, e.getCause());
		}
	finally
		{
		Var.popThreadBindings();
		}
	return ret;
}
