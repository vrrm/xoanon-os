(load "sim.lisp")
(load "prom.lisp")

(defclass octet-stream ()
  ((buffer :accessor buffer :initarg :buffer :type simple-octet-vector)))


(eval-when (:compile-toplevel :load-toplevel :execute)
  (defvar *binary-input-stream-class*
    (quote
     #+lispworks stream:fundamental-binary-input-stream
     #+sbcl sb-gray:fundamental-binary-input-stream
     #+openmcl gray:fundamental-binary-input-stream
     #+cmu ext:fundamental-binary-input-stream
     #+allegro excl:fundamental-binary-input-stream
     #-(or lispworks sbcl openmcl cmu allegro)
     (error "octet streams not supported in this implementation"))))

(defclass octet-input-stream (octet-stream #.*binary-input-stream-class*)
  ((index :accessor index :initarg :index :type index)
   (end :accessor end :initarg :end :type index)))

(defvar *stream-read-byte-function*
  (quote
   #+lispworks stream:stream-read-byte
   #+sbcl sb-gray:stream-read-byte
   #+openmcl gray:stream-read-byte
   #+cmu ext:stream-read-byte
   #+allegro excl:stream-read-byte
   #-(or lispworks sbcl openmcl cmu allegro)
   (error "octet streams not supported in this implementation")))

(defmethod #.*stream-read-byte-function* ((stream octet-input-stream))
  (let ((buffer (buffer stream))
        (index (index stream)))
    (declare (type (simple-array (unsigned-byte 8) (*)) buffer))
    (cond
      ((>= index (end stream)) :eof)
      (t
       (setf (index stream) (1+ index))
       (aref buffer index)))))


(defun make-octet-input-stream (buffer &optional (start 0) end)
  "As MAKE-STRING-INPUT-STREAM, only with octets instead of characters."
  (declare (type (simple-array (unsigned-byte 8) (*)) buffer))
  ;;           (type index start)
  ;;           (type (or index cl:null) end))
  (let ((end (or end (length buffer))))
    (make-instance 'octet-input-stream
                   :buffer buffer :index start :end end)))

(defun make-octet-input-stream (buffer &optional (start 0) end)
  "As MAKE-STRING-INPUT-STREAM, only with octets instead of characters."
  (declare (type (simple-array (unsigned-byte 8) (*)) buffer))
  (let ((end (or end (length buffer))))
    (make-instance 'octet-input-stream
                   :buffer buffer :index start :end end)))

(defun make-word-input-stream (octet-stream)
  "As MAKE-STRING-INPUT-STREAM, only with LM word (48bit) instead of characters."
  (make-instance 'word-input-stream
                   :buffer octet-stream :index (start octet-stream)  :end (end octet-stream)))

(MAKE-OCTET-INPUT-STREAM (make-array 5 :element-type '(unsigned-byte 8)))
(make-array  5 :element-type '(unsigned-byte 8) :initial-contents '(1 2 3 4 5))
(MAKE-OCTET-INPUT-STREAM (make-array 5 :element-type '(unsigned-byte 8) :initial-contents '(1 2 3 4 5)))
(STREAM-READ-BYTE (MAKE-OCTET-INPUT-STREAM (make-array 5 :element-type '(unsigned-byte 8) :initial-contents '(1 2 3 4 5))))




;; ; in: DEFUN MAKE-OCTET-INPUT-STREAM
;; ;     (SB-INT:NAMED-LAMBDA MAKE-OCTET-INPUT-STREAM
;; ;         (BUFFER &OPTIONAL (START 0) END)
;; ;       (DECLARE (TYPE (SIMPLE-ARRAY (UNSIGNED-BYTE 8) (*)) BUFFER))
;; ;       (BLOCK MAKE-OCTET-INPUT-STREAM
;; ;         (LET ((END #))
;; ;           (MAKE-INSTANCE 'OCTET-INPUT-STREAM :BUFFER BUFFER :INDEX START :END
;; ;                          END))))
;; ; 
;; ; caught STYLE-WARNING:
;; ;   undefined type: INDEX
;; ; 
;; ; compilation unit finished
;; ;   Undefined type:
;; ;     INDEX
;; ;   caught 1 STYLE-WARNING condition

;; (defvar *stream-read-byte-function*
;; 	       (quote
;; 		#+lispworks stream:stream-read-byte
;; 		#+sbcl sb-gray:stream-read-byte
;; 		#+openmcl gray:stream-read-byte
;; 		#+cmu ext:stream-read-byte
;; 		#+allegro excl:stream-read-byte
;; 		#-(or lispworks sbcl openmcl cmu allegro)
;; 		(error "octet streams not supported in this implementation")))

;; (defvar *stream-read-sequence-function*
;;   (quote
;;    #+lispworks stream:stream-read-sequence
;;    #+sbcl sb-gray:stream-read-sequence
;;    #+openmcl ccl:stream-read-vector
;;    #+cmu ext:stream-read-sequence
;;    #+allegro excl:stream-read-sequence
;;    #-(or lispworks sbcl openmcl cmu allegro)
;;    (error "octet streams not supported in this implementation")))

;; (defmethod #.*stream-read-byte-function* ((stream octet-input-stream))
;;   (let ((buffer (buffer stream))
;;         (index (index stream)))
;;     (declare (type (simple-array (unsigned-byte 8) (*)) buffer))
;;     (cond
;;       ((>= index (end stream)) :eof)
;;       (t
;;        (setf (index stream) (1+ index))
;;        (aref buffer index)))))

;; (STREAM-READ-BYTE (MAKE-OCTET-INPUT-STREAM (make-array 5 :element-type '(unsigned-byte 8) :initial-contents '(1 2 3 4 5))))
