(defvar code '(


(defun my-eval (exp env)
  (cond ((self-evaluatingp exp) exp)
        ((variablep exp) (lookup-variable-value exp env))
        ((quotedp exp) (text-of-quotation exp))
        ((assignmentp exp) (eval-assignment exp env))
        ((definitionp exp) (eval-definition exp env))
        ((ifp exp) (eval-if exp env))
        ((lambdap exp)
         (make-procedure (lambda-parameters exp)
                         (lambda-body exp)
                         env))
        ((beginp exp) 
         (eval-sequence (begin-actions exp) env))
        ((condp exp) (my-eval (cond->if exp) env))
        ((applicationp exp)
         (my-apply (my-eval (operator exp) env)
                   (list-of-values (operands exp) env)))
        (else
         (error "Unknown expression type -- EVAL"))))


(defun my-apply (procedure arguments)
  (cond ((primitive-procedurep procedure)
         (apply-primitive-procedure procedure arguments))
        ((compound-procedurep procedure)
         (eval-sequence
          (procedure-body procedure)
          (extend-environment
           (procedure-parameters procedure)
           arguments
           (procedure-environment procedure))))
        (else
         (error
          "Unknown procedure type -- APPLY"))))



(defun list-of-values (exps env)
  (if (no-operandsp exps)
      '()
    (cons (my-eval (first-operand exps) env)
          (list-of-values (rest-operands exps) env))))


(defun eval-if(exp env)
  (if (truep (my-eval (if-predicate exp) env))
      (my-eval (if-consequent exp) env)
    (my-eval (if-alternative exp) env)))


(defun eval-sequence (exps env)
  (cond ((last-expp exps) (my-eval (first-exp exps) env))
        (else (my-eval (first-exp exps) env)
              (eval-sequence (rest-exps exps) env))))

(defun eval-assignment (exp env)
  (set-variable-value! (assignment-variable exp)
                       (my-eval (assignment-value exp) env)
                       env)
  'ok)




(defun eval-definition (exp env)
  (define-variable! (definition-variable exp)
    (my-eval (definition-value exp) env)
    env)
  'ok)

(defun self-evaluatingp (exp)
  "atoms that evaluate back to themselves."
  (cond ((numberp exp) t)
        ((stringp exp) t)
        (t nil)))

;; ¤ Variables are represented by symbols:

(defun variablep (exp) (symbolp exp))

;; ¤ Quotations have the form (quote <text-of-quotation>):9

(defun quotedp (exp)
  (tagged-listp exp 'quote))

(defun text-of-quotation (exp) (cadr exp))

;; Quotedp is defined in terms of the procedure tagged-list-p, which
;; identifies lists beginning with a designated symbol:

(defun tagged-listp (exp tag)
  (if (pairp exp)
      (equal (car exp) tag)
    false))

;; ¤ Assignments have the form (set! <var> <value>):

(defun assignmentp (exp)
  (tagged-listp exp 'set!))

(defun assignment-variable (exp) (cadr exp))
(defun assignment-value (exp) (caddr exp))

;; ¤ Definitions have the form

;; (defvar <var> <value>)

;; or the form

;;(defun (<var> <parameter1> ... <parametern>)
;;  <body>)

;; The latter form (standard procedure definition) is syntactic sugar for

;;(defvar <var>
;;  (lambda (<parameter1> ... <parametern>)
                                        ; ;   <body>))

;; The corresponding syntax procedures are the following:

(defun definitionp (exp)
  (tagged-listp exp 'define))

(defun definition-variable (exp)
  (if (symbolp (cadr exp))
      (cadr exp)
    (caadr exp)))

(defun definition-value (exp)
  (if (symbolp (cadr exp))
      (caddr exp)
    (make-lambda (cdadr exp)   ; formal parameters
                 (cddr exp)))) ; body

;; ¤ Lambda expressions are lists that begin with the symbol lambda:

(defun lambdap (exp) (tagged-listp exp 'lambda))
(defun lambda-parameters (exp) (cadr exp))
(defun lambda-body (exp) (cddr exp))

;; We also provide a constructor for lambda expressions, which is used
;; by definition-value, above:

(defun make-lambda (parameters body)
  (cons 'lambda (cons parameters body)))

;; ¤ Conditionals begin with if and have a predicate, a consequent,
;; and an (optional) alternative. If the expression has no alternative
;; part, we provide false as the alternative.10

(defun ifp (exp) (tagged-listp exp 'if))
(defun if-predicate (exp) (cadr exp))
(defun if-consequent (exp) (caddr exp))

(defun if-alternative (exp)
  (if (not (nullp (cdddr exp)))
      (cadddr exp)
    'false))

;; We also provide a constructor for if expressions, to be used by
;; cond->if to transform cond expressions into if expressions:

(defun make-if (predicate consequent alternative)
  (list 'if predicate consequent alternative))

;; ¤ Begin packages a sequence of expressions into a single
;; expression. We include syntax operations on begin expressions to
;; extract the actual sequence from the begin expression, as well as
;; selectors that return the first expression and the rest of the
;; expressions in the sequence.11

(defun beginp (exp) (tagged-listp exp 'begin))
(defun begin-actions (exp) (cdr exp))
(defun last-expp (seq) (nullp (cdr seq)))
(defun first-exp (seq) (car seq))
(defun rest-exps (seq) (cdr seq))

;; We also include a constructor sequence->exp (for use by cond->if)
;; that transforms a sequence into a single expression, using begin if
;; necessary:

(defun sequence->exp (seq)
  (cond ((nullp seq) seq)
        ((last-expp seq) (first-exp seq))
        (else (make-begin seq))))
(defun make-begin (seq) (cons 'begin seq))

;; ¤ A procedure application is any compound expression that is not
;; one of the above expression types. The car of the expression is the
;; operator, and the cdr is the list of operands:

(defun applicationp (exp) (pairp exp))
(defun operator (exp) (car exp))
(defun operands (exp) (cdr exp))
(defun no-operandsp (ops) (nullp ops))
(defun first-operand (ops) (car ops))
(defun rest-operands (ops) (cdr ops))

;; Derived expressions

;; Some special forms in our language can be defined in terms of
;; expressions involving other special forms, rather than being
;; implemented directly. One example is cond, which can be implemented
;; as a nest of if expressions. For example, we can reduce the problem
;; of evaluating the expression

;; (cond ((> x 0) x)
;;       ((= x 0) (display 'zero) 0)
;;       (else (- x)))

;; to the problem of evaluating the following expression involving if and begin expressions:

;; (if (> x 0)
;;     x
;;     (if (= x 0)
;;         (begin (display 'zero)
;;                0)
;;         (- x)))

;; Implementing the evaluation of cond in this way simplifies the
;; evaluator because it reduces the number of special forms for which
;; the evaluation process must be explicitly specified.

;; We include syntax procedures that extract the parts of a cond
;; expression, and a procedure cond->if that transforms cond
;; expressions into if expressions. A case analysis begins with cond
;; and has a list of predicate-action clauses. A clause is an else
;; clause if its predicate is the symbol else.12

(defun condp (exp) (tagged-listp exp 'cond))
(defun cond-clauses (exp) (cdr exp))

(defun cond-else-clausep (clause)
  (equal (cond-predicate clause) 'else))

(defun cond-predicate (clause) (car clause))
(defun cond-actions (clause) (cdr clause))
(defun cond->if (exp)
  (expand-clauses (cond-clauses exp)))

(defun expand-clauses (clauses)
  (if (nullp clauses)
      'false                          ; no else clause
    (let ((first (car clauses))
          (rest (cdr clauses)))
      (if (cond-else-clausep first)
          (if (nullp rest)
              (sequence->exp (cond-actions first))
            (error "ELSE clause isn't last -- COND->IF"))
        (make-if (cond-predicate first)
                 (sequence->exp (cond-actions first))
                 (expand-clauses rest))))))

;; Expressions (such as cond) that we choose to implement as syntactic
;; transformations are called derived expressions. Let expressions are
;; also derived expressions (see exercise 4.6).13


(defun primitive-procedurep (proc)
  (tagged-listp proc 'primitive))

(defun primitive-implementation (proc) (cadr proc))



(defvar primitive-procedures
  (list (list 'car (lambda (x) (car x)))
        (list 'cdr (lambda (x) (cdr x)))
        (list 'cons (lambda (x y) (cons x y)))
        (list 'nullp (lambda (x) (null x)))
        ;;<more primitives>
        ))

(defun primitive-procedure-names ()
  (map (lambda (x) (car x))
       primitive-procedures))

(defun primitive-procedure-objects ()
  (map (lambda (proc) (list 'primitive (cadr proc)))
       primitive-procedures))

;; To apply a primitive procedure, we simply apply the implementation
;; procedure to the arguments, using the underlying Lisp system:17

(defun apply-primitive-procedure (proc args)
  (apply-in-underlying-scheme
   (primitive-implementation proc) args))

;; For convenience in running the metacircular evaluator, we provide a
;; driver loop that models the read-eval-print loop of the underlying
;; Lisp system. It prints a prompt, reads an input expression,
;; evaluates this expression in the global environment, and prints the
;; result. We precede each printed result by an output prompt so as to
;; distinguish the value of the expression from other output that may
;; be printed.18

(defvar input-prompt ";;; M-Eval input:")
(defvar output-prompt ";;; M-Eval value:")

(defun prompt-for-input (input-prompt)
  (let ((input (read)))
    (let ((output (my-eval input the-global-environment)))
      (announce-output output-prompt)
      (user-print output)))
  (driver-loop))

(defun prompt-for-input (string)
  (newline) (newline) (display string) (newline))

(defun announce-output (string)
  (newline) (display string) (newline))

;; We use a special printing procedure, user-print, to avoid printing
;; the environment part of a compound procedure, which may be a very
;; long list (or may even contain cycles).

(defun user-print (object)
  (if (compound-procedurep object)
      (display (list 'compound-procedure
                     (procedure-parameters object)
                     (procedure-body object)
                     '<procedure-env>))
    (display object)))


;; Operations on Environments

;; The evaluator needs operations for manipulating environments. As
;; explained in section 3.2, an environment is a sequence of frames,
;; where each frame is a table of bindings that associate variables
;; with their corresponding values. We use the following operations
;; for manipulating environments:


(defun enclosing-environment (env) (cdr env))
(defun first-frame (env) (car env))
(defvar the-empty-environment '())

;; Each frame of an environment is represented as a pair of lists: a
;; list of the variables bound in that frame and a list of the
;; associated values.14

(defun make-frame (variables values)
  (cons variables values))

(defun frame-variables (frame) (car frame))
(defun frame-values (frame) (cdr frame))

(defun add-binding-to-frame! (var val frame)
  (setq frame (cons var (car frame)))
  (setq frame (cons val (cdr frame))))

;; To extend an environment by a new frame that associates variables
;; with values, we make a frame consisting of the list of variables
;; and the list of values, and we adjoin this to the environment. We
;; signal an error if the number of variables does not match the
;; number of values.

(defun extend-environment (vars vals base-env)
  (if (= (length vars) (length vals))
      (cons (make-frame vars vals) base-env)
    (if (< (length vars) (length vals))
        (error "Too many arguments supplied")
      (error "Too few arguments supplied" ))))

;; To look up a variable in an environment, we scan the list of
;; variables in the first frame. If we find the desired variable, we
;; return the corresponding element in the list of values. If we do
;; not find the variable in the current frame, we search the enclosing
;; environment, and so on. If we reach the empty environment, we
;; signal an ``unbound variable'' error.

(defun lookup-variable-value (var env)
  (labels 
      ((env-loop (env)
                 (labels 
                     ((scan (vars vals)
                            (cond ((null vars)
                                   (env-loop (enclosing-environment env)))
                                  ((equal var (car vars))
                                   (car vals))
                                  (t (scan (cdr vars) (cdr vals))))
                            (if (equal env the-empty-environment)
                                (error "Unbound variable")
                              (let ((frame (first-frame env)))
                                (scan (frame-variables frame)
                                      (frame-values frame)
                                      (env-loop env)))))))))))

;; To set a variable to a new value in a specified environment, we
;; scan for the variable, just as in lookup-variable-value, and change
;; the corresponding value when we find it.

(defun set-variable-value (var val env)
  (labels 
      ((env-loop (env)
                 (labels 
                     ((scan (vars vals)
                            (cond ((nullp vars)
                                   (env-loop (enclosing-environment env)))
                                  ((equal var (car vars))
                                   (set-car! vals val))
                                  (else (scan (cdr vars) (cdr vals)))))
                      (if (equal env the-empty-environment)
                          (error "Unbound variable -- SET!")
                        (let ((frame (first-frame env)))
                          (scan (frame-variables frame)
                                (frame-values frame)))))
                   (env-loop env))))))

;; To define a variable, we search the first frame for a binding for
;; the variable, and change the binding if it exists (just as in
;; set-variable-value!). If no such binding exists, we adjoin one to
;; the first frame.

(defun define-variable (var val env)
  (let ((frame (first-frame env)))
    (lables ((scan (vars vals)
                   (cond ((nullp vars)
                          (add-binding-to-frame! var val frame))
                         ((equal var (car vars))
                          (set-car! vals val))
                         (else (scan (cdr vars) (cdr vals)))))
             (scan (frame-variables frame)
                   (frame-values frame))))))

(defun setup-environment ()
  (let ((initial-env
         (extend-environment (primitive-procedure-names)
                             (primitive-procedure-objects)
                             the-empty-environment)))
    (define-variable! 'true true initial-env)
    (define-variable! 'false false initial-env)
    initial-env))

                                        ;(defvar the-global-environment (setup-environment))

(defun run-tests (tests) 
  (mapcar (lambda (x) 
            (cons 
             (car x) 
             (funcall (caddr x) (list '()))))
          tests))

;;; tests should look like this:
;;; (<id> . (lambda (<env>) ...))
;;;      where env is a list of definitions needed to run 

(run-tests
 (list 
  (list 1 
        "Test setup environment"
        (lambda (env)
          ;; (let ((new-env (setup-environment)))
          ;;   (not (null new-env))))
          t
          )
        ;; (list 2
        ;;       "Test setup environment"
        ;;       (lambda (env)
        ;;         (let ((new-env (setup-environment)))
        ;;           (not (null new-env)))))
        ;; (list 3
        ;;       "Test setup environment"
        ;;       (lambda (env)
        ;;         (let ((new-env (setup-environment)))
        ;;           (not (null new-env)))))
        
        )))

 ;; Now all we need to do to run the evaluator is to initialize the
 ;; global environment and start the driver loop. Here is a sample
 ;; interaction:

 ;; (defvar the-global-environment (setup-environment))
 ;; (driver-loop)
;;; M-Eval input:
 ;; (defun (append x y)
 ;;   (if (nullp x)
 ;;       y
 ;;       (cons (car x)
 ;;             (append (cdr x) y))))
;;; M-Eval value:
 ;; ok
;;; M-Eval input:
 ;; (append '(a b c) '(d e f))
;;; M-Eval value:
 ;; (a b c d e f)


;;;;;; Environment Operations

 ;; (defclass environment ()
 ;;   ((the-environment :initform :accessor (setup-environment))))


 ;; ((radius :accessor circle-radius)
 ;; (center :accessor circle-center)))
))
 
