(defun run-eval-test ()
  (let ((my-env (run-env-test)))
    (assert (= (my-eval 'a my-env) 1))
    (assert (= (my-eval (list 'quote 1) my-env) 1))
    (my-eval (list 'set! 'a 2) my-env)
    (assert (= (my-eval 'a my-env) 2))
    (my-eval (list 'define 'x 3) my-env)
    (assert (= (my-eval 'x my-env) 3))
    (my-eval (list 'lambda (list 'la 'lb) (list '+ 'la 'lb)) my-env)
    (my-eval (list 'begin (list '+ 'a 'b)) my-env)
    (my-eval '(+ 1 2) my-env)
    ))

(defun my-eval (exp env)
  "ToDo -- Probably assignment should be based on a hashtable dispatch
   or even an array lookup. "
  (cond ((self-evaluatingp exp) exp)
        ((variablep exp) (lookup-variable-value exp env))
        ((quotedp exp) (text-of-quotation exp))
        ((assignmentp exp) (eval-assignment exp env))
        ((definitionp exp) (eval-definition exp env))
        ((ifp exp) (eval-if exp env))
        ((lambdap exp)
         (make-procedure (lambda-parameters exp)
                         (lambda-body exp)
                         env))
        ((beginp exp) 
         (eval-sequence (begin-actions exp) env))
        ((condp exp) (my-eval (cond->if exp) env))
        ((applicationp exp)
         (my-apply (my-eval (operator exp) env)
                   (list-of-values (operands exp) env)))
        (t
         (error "Unknown expression type -- EVAL"))))

(defun my-apply (procedure arguments)
  (cond ((primitive-procedurep procedure)
         (apply-primitive-procedure procedure arguments))
        ((compound-procedurep procedure)
         (eval-sequence
          (procedure-body procedure)
          (extend-environment
           (procedure-parameters procedure)
           arguments
           (procedure-environment procedure))))
        (t
         (error
          "Unknown procedure type -- MY-APPLY"))))

(defun list-of-values (exps env)
  (if (no-operandsp exps)
      '()
    (cons (my-eval (first-operand exps) env)
          (list-of-values (rest-operands exps) env))))


(defun eval-if(exp env)
  (if (truep (my-eval (if-predicate exp) env))
      (my-eval (if-consequent exp) env)
    (my-eval (if-alternative exp) env)))


(defun eval-sequence (exps env)
  (cond ((last-expp exps) (my-eval (first-exp exps) env))
        (t (my-eval (first-exp exps) env)
              (eval-sequence (rest-exps exps) env))))

(defun eval-assignment (exp env)
  (set-variable-value (assignment-variable exp)
                      (my-eval (assignment-value exp) env)
                      env)
  'ok)




(defun eval-definition (exp env)
  (define-variable (definition-variable exp)
    (my-eval (definition-value exp) env)
    env)
  'ok)

(defun self-evaluatingp (exp)
  "atoms that evaluate back to themselves."
  (cond ((numberp exp) t)
        ((stringp exp) t)
        (t nil)))

;; ¤ Variables are represented by symbols:

(defun variablep (exp) (symbolp exp))

;; ¤ Quotations have the form (quote <text-of-quotation>):9

(defun quotedp (exp)
  (tagged-listp exp 'quote))

(defun text-of-quotation (exp) (cadr exp))

;; Quotedp is defined in terms of the procedure tagged-listp, which
;; identifies lists beginning with a designated symbol:

(defun tagged-listp (exp tag)
  (if (listp exp)
      (equal (car exp) tag)
    '()))

;; ¤ Assignments have the form (set! <var> <value>):

(defun assignmentp (exp)
  (tagged-listp exp 'set!))

(defun assignment-variable (exp) (cadr exp))
(defun assignment-value (exp) (caddr exp))

;; ¤ Definitions have the form

;; (defvar <var> <value>)

;; or the form

;;(defun (<var> <parameter1> ... <parametern>)
;;  <body>)

;; The latter form (standard procedure definition) is syntactic sugar for

;;(defvar <var>
;;  (lambda (<parameter1> ... <parametern>)
                                        ; ;   <body>))

;; The corresponding syntax procedures are the following:

(defun definitionp (exp)
  (tagged-listp exp 'define))

(defun definition-variable (exp)
  (if (symbolp (cadr exp))
      (cadr exp)
    (caadr exp)))

(defun definition-value (exp)
  (if (symbolp (cadr exp))
      (caddr exp)
    (make-lambda (cdadr exp)   ; formal parameters
                 (cddr exp)))) ; body


(defun make-procedure (parameters body env)
  (list 'procedure parameters body env))

(defun compound-procedurep  (p)
  (tagged-listp p 'procedure))

(defun procedure-parameters (p) (cadr p))
(defun procedure-body  (p) (caddr p))
(defun procedure-environment (p) (cadddr p))

;; ¤ Lambda expressions are lists that begin with the symbol lambda:

(defun lambdap (exp) (tagged-listp exp 'lambda))
(defun lambda-parameters (exp) (cadr exp))
(defun lambda-body (exp) (cddr exp))

;; We also provide a constructor for lambda expressions, which is used
;; by definition-value, above:

(defun make-lambda (parameters body)
  (cons 'lambda (cons parameters body)))

;; ¤ Conditionals begin with if and have a predicate, a consequent,
;; and an (optional) alternative. If the expression has no alternative
;; part, we provide false as the alternative.10

(defun ifp (exp) (tagged-listp exp 'if))
(defun if-predicate (exp) (cadr exp))
(defun if-consequent (exp) (caddr exp))

(defun if-alternative (exp)
  (if (not (null (cdddr exp)))
      (cadddr exp)
    'false))

;; We also provide a constructor for if expressions, to be used by
;; cond->if to transform cond expressions into if expressions:

(defun make-if (predicate consequent alternative)
  (list 'if predicate consequent alternative))

;; ¤ Begin packages a sequence of expressions into a single
;; expression. We include syntax operations on begin expressions to
;; extract the actual sequence from the begin expression, as well as
;; selectors that return the first expression and the rest of the
;; expressions in the sequence.11

(defun beginp (exp) (tagged-listp exp 'begin))
(defun begin-actions (exp) (cdr exp))
(defun last-expp (seq) (null (cdr seq)))
(defun first-exp (seq) (car seq))
(defun rest-exps (seq) (cdr seq))

;; We also include a constructor sequence->exp (for use by cond->if)
;; that transforms a sequence into a single expression, using begin if
;; necessary:

(defun sequence->exp (seq)
  (cond ((null seq) seq)
        ((last-expp seq) (first-exp seq))
        (t (make-begin seq))))
(defun make-begin (seq) (cons 'begin seq))

;; ¤ A procedure application is any compound expression that is not
;; one of the above expression types. The car of the expression is the
;; operator, and the cdr is the list of operands:

(defun applicationp (exp) (consp exp))
(defun operator (exp) (car exp))
(defun operands (exp) (cdr exp))
(defun no-operandsp (ops) (null ops))
(defun first-operand (ops) (car ops))
(defun rest-operands (ops) (cdr ops))

;; Derived expressions

;; Some special forms in our language can be defined in terms of
;; expressions involving other special forms, rather than being
;; implemented directly. One example is cond, which can be implemented
;; as a nest of if expressions. For example, we can reduce the problem
;; of evaluating the expression

;; (cond ((> x 0) x)
;;       ((= x 0) (display 'zero) 0)
;;       (else (- x)))

;; to the problem of evaluating the following expression involving if and begin expressions:

;; (if (> x 0)
;;     x
;;     (if (= x 0)
;;         (begin (display 'zero)
;;                0)
;;         (- x)))

;; Implementing the evaluation of cond in this way simplifies the
;; evaluator because it reduces the number of special forms for which
;; the evaluation process must be explicitly specified.

;; We include syntax procedures that extract the parts of a cond
;; expression, and a procedure cond->if that transforms cond
;; expressions into if expressions. A case analysis begins with cond
;; and has a list of predicate-action clauses. A clause is an else
;; clause if its predicate is the symbol else.12

(defun condp (exp) (tagged-listp exp 'cond))
(defun cond-clauses (exp) (cdr exp))

(defun cond-else-clausep (clause)
  (equal (cond-predicate clause) 'else))

(defun cond-predicate (clause) (car clause))
(defun cond-actions (clause) (cdr clause))
(defun cond->if (exp)
  (expand-clauses (cond-clauses exp)))

(defun expand-clauses (clauses)
  (if (null clauses)
      'false                          ; no else clause
    (let ((first (car clauses))
          (rest (cdr clauses)))
      (if (cond-else-clausep first)
          (if (null rest)
              (sequence->exp (cond-actions first))
            (error "ELSE clause isn't last -- COND->IF"))
        (make-if (cond-predicate first)
                 (sequence->exp (cond-actions first))
                 (expand-clauses rest))))))

;; Expressions (such as cond) that we choose to implement as syntactic
;; transformations are called derived expressions. Let expressions are
;; also derived expressions (see exercise 4.6).13


(defun primitive-procedurep (proc)
  (tagged-listp proc 'primitive))

(defun primitive-implementation (proc) (cadr proc))


;; Note, we put values in a list to keep nil value (which is a valid
;; value of a symbol, from screwing things up.
(defvar primitive-procedures
  (list (list 'car (lambda (x) (car x)))
        (list 'cdr (lambda (x) (cdr x)))
        (list 'cons (lambda (x y) (cons x y)))
        (list 'null (lambda (x) (null x)))
        (list '+ (lambda (x y) (+ x y)))
        ;;<more primitives>
        ))

(defun primitive-procedure-names ()
  (mapcar (lambda (x) (car x))
          primitive-procedures))

(defun primitive-procedure-objects ()
  (mapcar (lambda (proc) (list (list 'primitive (cadr proc))))
          primitive-procedures))

;; To apply a primitive procedure, we simply apply the implementation
;; procedure to the arguments, using the underlying Lisp system:17

(defun apply-primitive-procedure (proc args)
  (apply
   (primitive-implementation proc) args))

;; For convenience in running the metacircular evaluator, we provide a
;; driver loop that models the read-eval-print loop of the underlying
;; Lisp system. It prints a prompt, reads an input expression,
;; evaluates this expression in the global environment, and prints the
;; result. We precede each printed result by an output prompt so as to
;; distinguish the value of the expression from other output that may
;; be printed.18

(defvar input-prompt ";;; M-Eval input:")
(defvar output-prompt ";;; M-Eval value:")

(defun prompt-for-input (input-prompt)
  (let ((input (read)))
    (let ((output (my-eval input the-global-environment)))
      (announce-output output-prompt)
      (user-print output)))
  (driver-loop))

(defun prompt-for-input (string)
  (newline) (newline) (display string) (newline))

(defun announce-output (string)
  (newline) (display string) (newline))

;; Watchout for cyles!
(defun user-print (object)
  (if (compound-procedurep object)
      (display (list 'compound-procedure
                     (procedure-parameters object)
                     (procedure-body object)
                     '<procedure-env>))
    (display object)))


;; Operations on Environments

;; The evaluator needs operations for manipulating environments. As
;; explained in section 3.2, an environment is a sequence of frames,
;; where each frame is a table of bindings that associate variables
;; with their corresponding values. We use the following operations
;; for manipulating environments:


(defun enclosing-environment (env) (cdr env))
(defun first-frame (env) (car env))
(defvar the-empty-environment (list '() '()))

;; Each frame of an environment is represented as a pair of lists: a
;; list of the variables bound in that frame and a list of the
;; associated values.14

(defun make-frame (variables values)
  (if (null variables)
      (list (list '()) (list '()))
    (cons variables values)))

(defun frame-variables (frame) (car frame))
(defun frame-values (frame) (car (cdr frame)))

(defun add-variable-symbol-to-frame (symbol frame)
  (set-car! 
   frame
   (cons symbol (car frame))))

(defun add-variable-value-to-frame (val old-frame)
  (let ((frame (cdr old-frame)))
    (set-car! 
     frame
     (cons (list val) (car frame)))))


(defun add-binding-to-frame! (var val frame)
  (add-variable-symbol-to-frame var frame)
  (add-variable-value-to-frame val frame))

;; To extend an environment by a new frame that associates variables
;; with values, we make a frame consisting of the list of variables
;; and the list of values, and we adjoin this to the environment. We
;; signal an error if the number of variables does not match the
;; number of values.

(defun extend-environment-with-null-frame (base-env)
  (cons (make-frame vars vals) base-env))

(defun extend-environment (vars vals base-env)
  (if (or (and (null vars) (null vals))
          (= (length vars) (length vals)))
      (cons (list vars vals)
            base-env)
    (if (< (length vars) (length vals))
        (error "Too many arguments supplied")
      (error "Too few arguments supplied" ))))

;; To look up a variable in an environment, we scan the list of
;; variables in the first frame. If we find the desired variable, we
;; return the corresponding element in the list of values. If we do
;; not find the variable in the current frame, we search the enclosing
;; environment, and so on. If we reach the empty environment, we
;; signal an ``unbound variable'' error.

(defun the-empty-environmentp (env)
  (equal env the-empty-environment))

(defun lookup-variable-value (var env)
  (labels 
      ((scan (vars vals)
             (cond ((null vars)
                    (env-loop (enclosing-environment env)))
                   ((equal var (car vars))
                    (caar vals))
                   (t (scan (cdr vars) (cdr vals)))))
       (env-loop (env)
                 (if (equal env the-empty-environment)
                     (error "Unbound variable")
                   (let ((frame (first-frame env)))
                     (scan (frame-variables frame)
                           (frame-values frame))))))
    (env-loop env)))

(defun set-car! (l val)
  "Set the value of the the first element of list l to val"
  (setf (car l) val))

(defun set-cdr! (l val)
  "Set the value of the the first element of list l to val"
  (setf (cdr l) (cons (car l) val)))

;; To set a variable to a new value in a specified environment, we
;; scan for the variable, just as in lookup-variable-value, and change
;; the corresponding value when we find it.

(defun run-env-test ()
  (let ((init-env (setup-environment)))
    (define-variable 'a 1 init-env)
    (let ((my-env (extend-environment '() '() init-env )))
      (define-variable 'b 2 my-env)
      (set-variable-value  'b 3 my-env)
      (lookup-variable-value 'b my-env)
      (lookup-variable-value 'a my-env)
      (assert (and (= (lookup-variable-value 'b my-env) 3)
                   (= (lookup-variable-value 'a my-env) 1)))
      my-env
      )))
  
(defun set-variable-value (var val env)
  "Note that the rules for looking up variable is different than the
   rules for defining them and differs also between scheme and lisp."
  (labels 
      ((scan (vars vals)
             (cond ((null vars)
                    (env-loop (enclosing-environment env)))
                   ((equal var (car vars))
                    (set-car! vals (list val)))
                   (t (scan (cdr vars) (cdr vals)))))
       (env-loop (env)
                 (if (equal env the-empty-environment)
                     (error "Unbound variable -- SET!")
                   (let ((frame (first-frame env)))
                     (scan (frame-variables frame)
                           (frame-values frame))))))
    (env-loop env)))

;; To define a variable, we search the first frame for a binding for
;; the variable, and change the binding if it exists (just as in
;; set-variable-value). If no such binding exists, we adjoin one to
;; the first frame.

(defun define-variable (var val env)
  (let ((frame (first-frame env)))
    (labels ((scan (vars vals)
                   (cond ((null vars)
                          (add-binding-to-frame! var val frame))
                         ((equal var (car vars))
                          (set-car! vals  (list val)))
                         (t (scan (cdr vars) (cdr vals))))))
      (scan (frame-variables frame)
            (frame-values frame)))))

(defun setup-environment ()
  (let ((initial-env
         (extend-environment (primitive-procedure-names)
                             (primitive-procedure-objects)
                             the-empty-environment)))
    (define-variable 'true t initial-env)
    (define-variable 'false nil initial-env)
    initial-env))

(defvar the-global-environment) 
(setq the-global-environment (setup-environment))

;; Now all we need to do to run the evaluator is to initialize the
;; global environment and start the driver loop. Here is a sample
;; interaction:

;; (defvar the-global-environment (setup-environment))
;; (driver-loop)
;;; M-Eval input:
;; (defun (append x y)
;;   (if (nullp x)
;;       y
;;       (cons (car x)
;;             (append (cdr x) y))))
;;; M-Eval value:
;; ok
;;; M-Eval input:
;; (append '(a b c) '(d e f))
;;; M-Eval value:
;; (a b c d e f)


;;;;;; Environment Operations

;; (defclass environment ()
;;   ((the-environment :initform :accessor (setup-environment))))


;; ((radius :accessor circle-radius)
;; (center :accessor circle-center)))


;; ((radius :accessor circle-radius)
;; (center :accessor circle-center)))


