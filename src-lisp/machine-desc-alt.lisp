
(defun org-xor (a b)
   "Exclusive or."
(if a (not b) b))
