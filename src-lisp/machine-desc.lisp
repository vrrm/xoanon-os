
;;(in-package #:sim)

;;(load "sim.lisp")
;;(load "prom.lisp")

;;(print "my gcd-machine")

;; (let ((gcd-machine
;;        (make-machine
;; 	'(a b t)
;; 	(list (list 'rem (lambda (x y) (remainder x y)))
;; 	      (list '= (lambda (x y)  (= x y))))
;; 	'(test-b
;; 	  (test (op =) (reg b) (const 0))
;; 	  (branch (label gcd-done))
;; 	  (assign t (op rem) (reg a) (reg b))
;; 	  (assign a (reg b))
;; 	  (assign b (reg t))
;; 	  (goto (label test-b))
;; 	  gcd-done)
;; 	nil
;; 	)))
  ;; (set-register-contents! gcd-machine 'a 206)
  ;; (set-register-contents! gcd-machine 'b 40)
  ;; (print (list "gcd of "
  ;; 	       (get-register-contents gcd-machine 'a)
  ;; 	       " and "
  ;; 	       (get-register-contents gcd-machine 'b)
  ;; 	       " are "))
  ;; (start gcd-machine)
  ;; (get-register-contents gcd-machine 'a)
  ;; (print (list "result: " (get-register-contents gcd-machine 'a)))
;;  )
(defvar registers
  '(

;;;
    a
    b
    t

    q


    ))



;; * Buses: A, B, OB


;; (defvar bin-instructions
;;   (get-prom))

;; (defun allocate-bin-instructions ()
;;   (get-prom))

(defvar instructions
   '(test-b
     (test (op =) (reg b) (const 0))
     (branch (label gcd-done))
     (assign t (op rem) (reg a) (reg b))
     (assign a (reg b))
     (assign b (reg t))
     (goto (label test-b))
     gcd-done))


(defvar ops
   (list
    (list 'rem
          (lambda (x y)
            (remainder x y)))
    (list '=
          (lambda (x y)
            (= x y)))))

(print "my-cadr")
(defun run-my-cadr ()
  (let ((my-cadr
	  (make-machine
	    registers
	    ops
	    ;;instructions
	    nil
	    (get-prom)
	   ;; nil
	    )))
    (set-register-contents! my-cadr 'a 56)
    (set-register-contents! my-cadr 'b 14)
    (start my-cadr)
    (cycle my-cadr 1)
    ))

;;(get-register-contents my-cadr 'a)


;;(print (get-register-contents my-cadr 'a))

;; (funcall (funcall my-cadr 'stack) 'print-statistics)
