;;;; cadr-emulator.lisp

(in-package #:cadr-emulator)

(define-foreign-library lisp-main
  (t (:default "lisp-main")))

(load-foreign-library "/home/rett/dev/cadr-emulator/lisp-main.so")

(defctype ucw_t :unsigned-long)

(defcfun "lisp_main" :int)
(defcfun "display_init" :int)
(defcfun "display_poll" :int)
(defcfun "config_get_disk_filename" :pointer)
(defcfun "disk_init" :int (filename :pointer))
(defcfun "display_init" :int)
(defcfun "read_prom_files" :int)
(defcfun "read_sym_files" :int)
(defcfun "iob_init" :int)
(defcfun "chaos_init" :int)
(defcfun "ether_init" :int)
(defcfun "show_prom" :int)
(defcfun "disassemble_prom" :void)
(defcfun "signal_init" :void)
(defcfun "show_prom" :int)
(defcfun "get_warm_boot_flag" :int)
(defcfun "iob_warm_boot_key" :void)
(defcfun "lisp_run" :int)
(defcfun "wrap_set_breakpoints" :void)
(defcfun "patch_prom_code" :void)
(defcfun "write_phy_mem" :int (paddr :int) (v :unsigned-int ))
(defcfun "timing_start" :void)
(defcfun "post_run" :int)
(defcfun "get_run_ucode_flag" :int)
(defcfun "get_cycles" :unsigned-long)
(defcfun "set_p0" ucw_t (val ucw_t))
(defcfun "get_p0" ucw_t)
(defcfun "set_p1" ucw_t (val ucw_t))
(defcfun "get_p1" ucw_t)
(defcfun "set_p0_pc" :int (val :int))
(defcfun "get_p0_pc" ucw_t)
(defcfun "set_p1_pc" :int (val :int))
(defcfun "get_p1_pc" ucw_t)
(defcfun "get_u_pc" :int)
(defcfun "inc_u_pc" :int)
(defcfun "set_no_exec_next" :char (val :char))
(defcfun "run_lashup" :void)
(defcfun "iob_poll" :void)
(defcfun "disk_poll" :void)
(defcfun "amp" :unsigned-long (a :unsigned-long) (b :unsigned-long))
(defcfun "amp_x_0x0ffff" :unsigned-long (a :unsigned-long) )
(defcfun "display_poll" :void)
(defcfun "chaos_poll" :void)
(defcfun "ether_poll" :void)
(defcfun "fetch" ucw_t)
(defcfun "get_new_md_delay" :int)
(defcfun "set_new_md_delay" :int (val :int))
(defcfun "dec_new_md_delay" :int )
(defcfun "set_md" :unsigned-int (val :unsigned-int))
(defcfun "md_delay_block" :void)








(defvar u_pc)
(defvar interrupt_status_reg)


(defun process-arg ()
  (list
   '("a" (enable-flag 'alt-prom-flag))
   '("b" (breakpoint-set-mcr optarg))
   '("B" (set-c-global begin-trace-cycle (atol optarg)))
   '("c" (set-c-global max-cycles (atol optarg)))
   '("C" (set-c-global max-trace-cycles (atol optarg)))
   '("d" (enable-flag 'dump-state-flag))
   '("i" (config-set-disk-filename optarg))
   '("l" (tracelabel-set-mcr optarg))
   '("n" (disable-flag 'show-video-flag))
   '("m" (disable-flag 'mouse-sync-flag))
   '("p" (breakpoint-set-prom optarg))
   '("q" (breakpoint-set-count (atoi optarg)))
   '("r" (mam-sit-tree-directory))
   '("S" (enable-flag 'save-state-flag))
   '("t" (enable-flag 'trace))
   '("ta" (enable-flag 'trace-after-flag))
   '("td" (enable-flag 'trace-disk-flag))
   '("ti" (enable-flag 'trace-int-flag))
   '("to" (enable-flag 'trace-io-flag))
   '("tp" (enable-flag 'trace-prom-flag))
   '("tc" (enable-flag 'trace-mcr-flag))
   '("tm" (enable-flag 'trace-mcr-labels-flag))
   '("tn" (enable-flag 'trace-net-flag))
   '("tl" (enable-flag 'trace-lod-labels-flag))
   '("tv" (enable-flag 'trace-vm-flag))
   '("s" (enable-flag 'stop-after-prom-flag))
   '("w" (enable-flag 'warm-boot-flag))
   ))

(defun pre-lisp-main ()
  (print "CADR emulator v0.9")
  (display-init)
  (display-poll)
  (disk-init
   (config-get-disk-filename))

  (read-prom-files)
  (read-sym-files)
  (iob-init)
  (chaos-init)
  (ether-init)
  (show-prom)
  (disassemble-prom)
  (signal-init)
  (cond ((/= (get-warm-boot-flag) 0)
         (iob-warm-boot-key)))

  (wrap-set-breakpoints)
  (print "run:")
  (patch-prom-code)
  (write-phy-mem 0 0)
  (timing-start)
  (run)
  (post-run)
  (lisp-main)  
  )


(defun run () 
  (loop while (/= (get-run-ucode-flag) 0) do
       (cond ((eql (get-cycles) 0)
              (set-p0 0)
              (set-p1 0)
              (set-p0-pc 0)
              (set-p1-pc 0)
              (set-no-exec-next 0)))
       (cond ((= (amp-x-0x0ffff (get-cycles)) 0)
              (display-poll)
              (chaos-poll)
              (ether-poll)))
       (run-lashup)
       (iob-poll)
       (disk-poll)
       (set-p0 (get-p1))
       (set-p0-pc (get-p1-pc))
       (set-p1 (fetch))
       (set-p1-pc (get-u-pc))
       (inc-u-pc)
       ;; (cond ((> (get-new-md-delay) 0)
       ;;        (dec-new-md-delay)
       ;;        (cond ((= (get-new-md-delay) 0)
       ;;               (set-md (get-new-md))))))
       (md-delay-block)
       

;;; -----------------------------------------
       (lisp-run)
       ))


;; for now
(let ((pack (find-package :cadr-emulator)))
(do-all-symbols (sym pack) (when (eql (symbol-package sym) pack) (export sym))))


;;(load "ucode.lisp")
;;(demo:pre-lisp-main)

; (& "0140000" '(21 3))

;;(defvar interrupt-pending-flag (u_pc)
;;  (interrupt_status_reg & 0140000) ? 1 : 0;)


;; (defun set_interrupt_status_reg (new);
;; 	(setq interrupt_status_reg new)
;; 	(setq interrupt_pending_flag  (cond ((eql (& "0140000" interrupt_status_reg) 0)
;;                                        0)
;;                                        ((1)))))
