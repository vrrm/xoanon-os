;;;; cadr-emulator.asd

(asdf:defsystem #:cadr-emulator
  :serial t
  :description "cadr-emulator emulator of the MIT CADR software in Lisp"
  :author "Your Name <vrrm00@gmail.com>"
  :license "MIT(ish)"
  :depends-on (#:cffi #:cl-ppcre)
  :components ((:file "package")
               (:file "bit-manipulation")
               (:file "cadr-emulator"
                      :depends-on ("bit-manipulation")
                      )
               (:file "test")
               ))
