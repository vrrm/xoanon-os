;; main()
;; {
;; 	int xbusl, xbush, unibusl, unibush, xbusiol, xbusioh;

;; 	printf("CADR memory map:\n");
;; 	xbusl = 0;
;; 	xbush = 016777777;
;; 	unibusl = 017400000;
;; 	unibush = 017777777;
;; 	xbusiol = 017000000;
;; 	xbusioh = 017377777;
;; 	printf("22-bit      %011o\n", (1 << 22)-1);
;; 	printf("xbus memory %011o-%011o, pn %05o-%05o\n",
;; 	       xbusl, xbush, xbusl >> 8, xbush>> 8);
;; 	printf("xbus io     %011o-%011o, pn %05o-%05o\n",
;; 	       xbusiol, xbusioh, xbusiol >> 8, xbusioh >> 8);
;; 	printf("unibus io   %011o-%011o, pn %05o-%05o\n",
;; 	       unibusl, unibush, unibusl >> 8, unibush >> 8);
;; }


;; Concatenate bitfields
(defun concatenate-bits (bits1 field-size1 bits2 field-size2)
  (dpb bits1 (byte (- field-size2 1) field-size1) bits2))

(defun compute-needed-fills (size)
  (labels ((compute-needed-fills-iter (size needed-fills)
             (if (zerop size)
                 needed-fills
                 (multiple-value-bind (qotient remainder) (floor (/ size 2))
                   (compute-needed-fills-iter 
                    qotient (cons (plusp remainder) 
                                  needed-fills))))))
    (compute-needed-fills-iter size '())))


(defun bv2int(z)
  "convert a bitvector to a positive integer. e.g. #*101 -> 5"
  (reduce #'(lambda(x y) (+ (* 2 x) y)) z))

(defun int2bv(i n)
  "convert a positive integer i to a bit vector of exactly length n"
  (let ((res (make-array n :element-type 'bit)))
    (dotimes (k n) (setf (bit res (- n k 1)) (mod i 2)
                         i (truncate i 2)))
    res)) 


;; create a bitfield of arbitrary size filed with 0's
(defun fill-bits2 (size)
  (let* ((init-bit-fields
          '(#b1           ;2^1
            #b11          ;2^2
            #b1111        ;2^3
            #b11111111))  ;2^4
         (needed-fills
          (compute-needed-fills size)))
    (labels (
             (generate-field (needed-fills bit-fields)
               (if (plusp (car needed-fills))
                   
                   (fill-bits-iter (bit-fields)
                                   (if (> (length needed-fills) (length bit-fields))
                                       (fill-bits-iter 
                                        (cons 
                                         (concatenate-bit-fields
                                          (car bit-fields)
                                          (car bit-fields))
                                         bit-fields))
                                       (generate-field needed-fills bit-fields))))
               (fill-bits-iter init-bit-fields))))))




;;  (make-array 5 :fill-pointer 0 :adjustable t :element-type 'integer))
;; (push bit-array #b1)

;; (cond ((>=  shift 0)
;;        (dpb (ldb (byte shift-size 0) int)
;;             (byte shift-size shift )
;;             0))
;;       ((< shift 0)
;;        (ldb (byte shift-size (- shift)) int)))))


;;  Like ash except that field-size is specified and shift is logical
;;  rather than arithmatic, i.e. leftmost bit of the field is never
;;  interpreted as a sign bit of two's complement.  Bits outside of
;;  the specified field are set to 0.
(defun logical-shift (int field-size shift)
  (let ((shift-size (- field-size (abs shift))))
    (cond ((>=  shift 0)
           (dpb (ldb (byte shift-size 0) int)
                (byte shift-size shift )
                0))
          ((< shift 0)
           (ldb (byte shift-size (- shift)) int)))))

(defun print-memory ()
  (let* ((xbusl #o0)
         (xbusl-sh (logical-shift xbusl 22 -8))
         (xbush #o016777777)
         (xbush-sh (logical-shift xbush 22 -8))
         (unibusl #o017400000)
         (unibusl-sh (logical-shift unibusl 22 -8))
         (unibush #O017777777)
         (unibush-sh (logical-shift unibush 22 -8))
         (xbusiol #O017000000)
         (xbusiol-sh (logical-shift xbusiol 22 -8))
         (xbusioh #o017377777)
         (xbusioh-sh (logical-shift xbusioh 22 -8))
         (octal-format-string "~11,'0o"))
    (format t "22-bit      ~11,'0o~%"   
            (bv2int (make-array 22 :element-type 'bit :initial-element 1)))
    (format t "xbus memory ~11,'0o-~11,'0o, pn ~5,'0o-~5,'0o~%"   
            xbusl xbush  xbusl-sh xbush-sh)
    (format t "xbus io     ~11,'0o-~11,'0o, pn ~5,'0o-~5,'0o~%"   
            xbusiol xbusioh xbusiol-sh xbusioh-sh)
    (format t "unibus io   ~11,'0o-~11,'0o, pn ~5,'0o-~5,'0o~%"   
            unibusl unibush unibusl-sh unibush-sh)
    ))

