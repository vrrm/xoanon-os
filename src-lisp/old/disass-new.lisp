;;; fef disassembler			-*-lisp-*-
;;;	** (c) copyright 1980 massachusetts institute of technology **
;;; this stuff is used by lispm2; eh >.  if you change things around,
;;; make sure not to break that program.



;;; 'Unwrap' uncapsulations until the unencapsulated function is foind
;;; and return that.  from encaps.lisp
;;
;; (defun unencapsulate-function-spec
;;        (function-spec &optional encapsulation-types
;;                       &aux def tem)
;;   (prog ()
;;     ;; If a symbol was specified as the type of encapsulation,
;;     ;; then process all types which come outside of that type,
;;     ;; not including that type itself.
;;     (and encapsulation-types (symbolp encapsulation-types)
;;          (or (setq encapsulation-types
;;                    (cdr (memq encapsulation-types encapsulation-standard-order)))
;;              (return function-spec)))
;;     (return
;;       (cond ((not (fdefinedp function-spec)) function-spec)
;;             ((and (progn (setq def (fdefinition function-spec))
;;                          (and (listp def) (eq (car def) 'macro)
;;                               (setq def (cdr def)))
;;                          (not (symbolp def)))
;;                   (setq tem (assq 'encapsulated-definition
;;                                   (function-debugging-info def)))
;;                   (or (null encapsulation-types)
;;                       (memq (caddr tem) encapsulation-types)))
;;              (unencapsulate-function-spec (cadr tem) encapsulation-types))
;;             (t function-spec)))))



;;; Defined in micro code: ucadr.lisp
;; XDATTP (MISC-INST-ENTRY %DATA-TYPE)
;;         (POPJ-AFTER-NEXT 
;;          (M-T)  C-PDL-BUFFER-POINTER-POP 
;;                 (A-CONSTANT (BYTE-VALUE Q-DATA-TYPE DTP-FIX))
;;                 Q-DATA-TYPE)
;;        (NO-OP)
(defun %data-type (ptr)
  ;; extract datatype byte from ptr
  'dtp-fef-pointer
)

(defvar disassemble-object-output-fun nil)
(defun disassemble (function &optional fef lim-pc ilen (disassemble-object-output-fun nil))
  (cond ((= (%data-type function) dtp-fef-pointer)
         (setq fef function))
        ((setq fef (fdefinition (si:unencapsulate-function-spec function)))))
  (cond ((and (listp fef)
              (eq (car fef) 'macro))
         (format t "~%Definition as macro")
         (setq fef (cdr fef))))
  (or (= (%data-type fef) dtp-fef-pointer)
      (ferror nil "can't find fef for ~s" function))
  (setq lim-pc (disassemble-lim-pc fef))
  (do pc (fef-initial-pc fef) (+ pc ilen) (>= pc lim-pc)
    (terpri)
    (setq ilen (disassemble-instruction fef pc)))
  (terpri)
  function)

(defun disassemble-lim-pc (fef &aux lim-pc)
  (setq lim-pc (* 2 (si:fef-length fef)))
  (cond ((zerop (disassemble-fetch fef (1- lim-pc)))
         (1- lim-pc))
        (t lim-pc)))

;; return the length of the instruction in fef at pc.
(defun disassemble-instruction-length (fef pc &aux wd op disp)
  (setq wd (disassemble-fetch fef pc))
  (setq op (ldb 1104 wd)
        disp (ldb 0011 wd))
  (cond ((and (= op 14) (= disp 777)) 2)
        (t 1)))

                                        ;returns the length of the instruction, usually 1. 
(defun disassemble-instruction (fef pc &aux &special (base 8) &local
                                         wd op dest reg disp)
  (prog nil  ;prog so that return can be used to return unusual instruction lengths. 
     (setq wd (disassemble-fetch fef pc))
     (prin1 pc)
     (tyo 40)
     (setq op (ldb 1104 wd)
           dest (ldb 1503 wd)
           disp (ldb 0011 wd)
           reg (ldb 0603 wd))
     (cond ((zerop wd)
            (princ "0"))
           ((< op 11)     ;dest/addr 
            (princ (nth op '(call call0 move car cdr cadr cddr cdar caar)))
            (tyo 40)
            (princ (nth dest '(d-ignore d-pdl d-next d-last
                               d-return d-next-q d-last-q d-next-list)))
            (disassemble-address fef reg disp))
           ((= op 11)	    ;nd1
            (princ (nth dest '(nd1-unused + - * // logand logxor logior)))
            (disassemble-address fef reg disp))
           ((= op 12)	    ;nd2
            (princ (nth dest '(= > < eq sete-cdr sete-cddr sete-1+ sete-1-)))
            (disassemble-address fef reg disp))
           ((= op 13)	    ;nd3
            (princ (nth dest '(bind-obsolete? bind-nil bind-pop set-nil set-zero push-e movem pop)))
            (disassemble-address fef reg disp))
           ((= op 14)	    ;branch
            (princ (nth dest '(br br-nil br-not-nil br-nil-pop
                               br-not-nil-pop br-atom br-not-atom br-ill-7)))
            (tyo 40)
            (and (> disp 400) (setq disp (logior -400 disp))) ;sign-extend
            (cond ((neq disp -1)	    ;one word
                   (prin1 (+ pc disp 1)))
                  (t	    ;long branch
                   (setq disp (disassemble-fetch fef (setq pc (1+ pc))))
                   (and (> disp 100000) (setq disp (logior -100000 disp)))
                   (princ "*")	;indicate long branch for user.
                   (prin1 (+ pc disp 1))
                   (return 2))))
           ((= op 15)	    ;misc
            (princ "(misc) ") ;moon likes to see this
            (cond ((< disp 100) (format t "list ~d long " disp))
                  ((< disp 200) (format t "list-in-area ~d long " (- disp 100)))
                  ((< disp 220)
                   (format t "unbind ~d binding~:p " (- disp 177))  ;code 200 does 1 unbind.
                   (and (zerop dest) (return 1)))
                  ((< disp 240)
                   (format t "pop-pdl ~d time~:p " (- disp 220)) ;code 220 does 0 pops.
                   (and (zerop dest) (return 1)))
                  (t
                   (let ((op (micro-code-symbol-name-area (- disp 200))))
                     (cond ((null op) (format t "#~o " disp))
                           (t (format t "~a " op))))))
            (princ (nth dest '(d-ignore d-pdl d-next d-last
                               d-return d-next-q d-last-q d-next-list))))
           (t		    ;undef
            (princ 'undef-)
            (prin1 op)))
     (return 1)))

;; print out the disassembly of an instruction source address.
;; reg is the register number of the address, and disp is the displacement.
(defun disassemble-address (fef reg disp &aux ptr offset tem loc cell)
  (tyo 40)
  (cond ((< reg 4)
         (setq loc (%make-pointer-offset dtp-locative fef disp))
         (format t "fef|~d ~30,8t;" disp)
         (cond ((= (%p-ldb-offset %%q-data-type fef disp) dtp-external-value-cell-pointer)
                (setq ptr (%find-structure-header
                           (setq tem (%p-contents-as-locative-offset fef disp)))
                      offset (%pointer-difference tem ptr))
                (cond ((symbolp ptr)
                       (setq cell (nth offset '("@+0?? " "" "#'"
                                                "@plist-head-cell " "@package-cell "))))
                      ((listp ptr)
                       (setq ptr (si:meth-function-spec ptr) cell "#'"))
                      (t (setq cell "")))
                (if disassemble-object-output-fun
                    (funcall disassemble-object-output-fun ptr cell loc)
                    (princ cell)
                    (prin1 ptr) ))
               (t
                (if disassemble-object-output-fun
                    (funcall disassemble-object-output-fun (car loc) "'" loc)
                    (princ '/')
                    (prin1 (%p-contents-offset fef disp))))))
        ((= reg 4)
         (princ '/')
         (prin1 (constants-area (logand 77 disp))))
        ((= disp 777)
         (princ 'pdl-pop))
        ((= reg 5)
         (format t "local|~d" (logand 77 disp))
         (setq tem (disassemble-local-name fef (logand 77 disp)))
         (and tem (format t " ~30,8t;~a" tem)))
        ((= reg 6)
         (format t "arg|~d" (logand 77 disp))
         (setq tem (disassemble-arg-name fef (logand 77 disp)))
         (and tem (format t " ~30,8t;~a" tem)))
        (t
         (format t "pdl|-~d" (logand 77 disp)))))

;; given a fef and the number of a slot in the local block,
;; return the name of that local (or nil if unknown).
;; if it has more than one name due to slot-sharing, we return a list of
;; the names, but if there is only one name we return it.
(defun disassemble-local-name (fef localnum)
  (let ((fdi (si:function-debugging-info fef)))
    (let ((names (nth localnum (cadr (assq 'compiler:local-map fdi)))))
      (cond ((null names) nil)
            ((null (cdr names)) (car names))
            (t names)))))

;; given a fef and the number of a slot in the argument block,
;; return the name of that argument (or nil if unknown).
;; first we look for an arg map, then we look for a name in the adl.
(defun disassemble-arg-name (fef argnum &aux
                                          (fdi (si:function-debugging-info fef))
                                          (argmap (cadr (assq 'compiler:arg-map fdi))))
  (cond (argmap (car (nth argnum argmap)))
        (t (do ((adl (get-macro-arg-desc-pointer fef) (cdr adl))
                (idx 0 (1+ idx))
                (adlword))
               ((null adl))
             (setq adlword (car adl))
             (select (mask-field %%fef-arg-syntax adlword)
                     ((fef-arg-req fef-arg-opt))
                     (otherwise (return)))
             (and (= 1 (ldb %%fef-name-present adlword))
                  (setq adl (cdr adl)))
             (cond ((= idx argnum)
                    (return (and (= 1 (ldb %%fef-name-present adlword)) (car adl)))))
             (select (mask-field %%fef-init-option adlword)
                     ((fef-ini-pntr fef-ini-c-pntr fef-ini-opt-sa fef-ini-eff-adr)
                      (setq adl (cdr adl))))))))

;; given a fef and a pc, returns the corresponding 16-bit macro instruction.
;; there is no error checking.
(defun disassemble-fetch (fef pc &aux idx)
  (setq idx (// pc 2))
  (cond ((zerop (logand 1 pc))
         (%p-ldb-offset %%q-low-half fef idx))
        ((%p-ldb-offset %%q-high-half fef idx))))
