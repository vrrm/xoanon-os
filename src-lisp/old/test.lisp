(in-package #:cadr-emulator)

(defun run-tests (tests) 
  ;; tests should look like this:
  ;; (<id> . (lambda (<env>) ...))
  ;;      where env is a list of definitions needed to run 
  (let  ((test-ints 
          '(2 
            4 
            11 
            29 
            34 
            67 
            216 
            230 
            2162 
            2430 
            7129 
            7161 
            103480 
            113229 
            5317346 
            5834423 
            6711801 
            7162837 
            7195565 
            7305418 
            34974120 
            34977304 
            74263024 
            117715152 
            118133040 
            118133040 
            118133064 
            118163192 
            738026112 
            35362848768))
         (tests
          (list 
           (list 2
                 "Test setup environment"
                 (lambda (env)
                   (mapc (lambda (a) (cond ((/= (amp-x-0x0ffff a) (& (list a 20) "0x0ffff"))
                                            (error "no match"))))
                         test-ints)
                   )
                 '(APPLY-PRIMITIVE-PROCEDURE PRIMITIVE-PROCEDURE)
                 )))))
  (mapcar (lambda (x) 
            (cons 
             (car x) 
             (funcall (caddr x) (list '()))))
            tests))

(defun file-string (path)
  "Sucks up an entire file from PATH into a freshly-allocated string,
      returning two values: the string and the number of bytes read."
  (with-open-file (s path)
                  (let* ((len (file-length s))
                         (data (make-string len)))
                    (values data (read-sequence data s)))))



(defun get-code-file ()
  (cadr(read-from-string (concatenate 'string  " '(" 
                                      (file-string "lisp-on-lisp-sicp.lisp")
                                      ")"))))

(defun file-forms (path)
  "Sucks up an entire file from PATH into a list of forms (sexprs),
      returning two values: the list of forms and the number of forms read."
  (with-open-file (s path)
                  (loop with eof = (list nil)
                        for form = (read s nil eof)
                        until (eq form eof)
                        collect form into forms
                        counting t into form-count
                        finally (return (values forms form-count)))))

(defun get-functions () 
  (remove nil (mapcar
               (lambda (x) 
                 (if (listp x)
                     (cadr x)))
               (get-code-file)) 
          ))


(defun get-functions-without-tests () 
  (let ((functions-without-tests '()))
    (maphash 
     (lambda (key value) 
       (push key functions-without-tests))
     (let ((hash-table (make-hash-table :test 'equal)))
       (mapcar
        (lambda (x)
          (setf (gethash x hash-table) x))
        (get-functions))
       (mapcar
        (lambda(test)
          (mapcar
           (lambda (function-name)
             (remhash function-name hash-table))
           (cadddr test)))
        tests)
       hash-table))
    functions-without-tests
    ))

