(asdf:oos 'asdf:load-op :cffi)

(cl:defpackage  :demo
  (:use
   :common-lisp
   :cffi
   ))
(cl:in-package :demo) 

(define-foreign-library lisp-main
  (t (:default "lisp-main")))


(load-foreign-library "/home/rett/Dev/lisp-machine/dev/usim/lisp-main.so")

(defcfun "lisp_main" :int)
(defcfun "display_init" :int)
(defcfun "display_poll" :int)
(defcfun "config_get_disk_filename" :pointer)
(defcfun "disk_init" :int (filename :pointer))
(defcfun "display_init" :int)
(defcfun "read_prom_files" :int)
(defcfun "read_sym_files" :int)
(defcfun "iob_init" :int)
(defcfun "chaos_init" :int)
(defcfun "ether_init" :int)
(defcfun "show_prom" :int)
(defcfun "disassemble_prom" :void)
(defcfun "signal_init" :void)
(defcfun "show_prom" :int)
(defcfun "get_warm_boot_flag" :int)
(defcfun "iob_warm_boot_key" :void)
(defcfun "lisp_run" :int)


;; global vars
;;  (defvar cycles '())
;; (defvar trace-cycles)
;; (defvar max-cycles)
;; (defvar max-trace-cycles)
;; (defvar begin-trace-cycle)

;; (defvar u-pc)
;; (defvar page-fault-flag)
;; (defvar interrupt-pending-flag)
;; (defvar interrupt-status-reg)

;; (defvar sequence-break-flag)
;; (defvar interrupt-enable-flag)
;; (defvar lc-byte-mode-flag)
;; (defvar bus-reset-flag)

;; (defvar prom-enabled-flag)
;; (defvar stop-after-prom-flag)
;; (defvar run-ucode-flag)
;; (defvar warm-boot-flag)
;; (defvar save-state-flag)
;; (defvar dump-state-flag)

;; (defvar md)
;; (defvar vma)
;; (defvar q)
;; (defvar opc)

;; (defvar new-md)
;; (defvar new-md-delay)

;; (defvar write-fault-bit)
;; (defvar access-fault-bit)

;; (defvar alu-carry)
;; (defvar alu-out)

;; (defvar oa-reg-lo)
;; (defvar oa-reg-hi)
;; (defvar oa-reg-lo-set)
;; (defvar oa-reg-hi-set)

;; (defvar interrupt-control)
;; (defvar dispatch-constant)

;; (defvar c-trace)
;; (defvar trace-mcr-labels-flag)
;; (defvar trace-lod-labels-flag)
;; (defvar trace-prom-flag)
;; (defvar trace-mcr-flag)
;; (defvar trace-io-flag)
;; (defvar trace-vm-flag)
;; (defvar trace-disk-flag)
;; (defvar trace-net-flag)
;; (defvar trace-int-flag)
;; (defvar trace-late-set)
;; (defvar trace-after-flag)

;; (defvar macro-pc-incrs)

;; (defvar phys-ram-pages)

;; (defvar pdl_ptr)
;; (defvar pdl_index)
;; (defvar lc)
;; (defvar spc_stack_ptr)

;; (defvar alu-stat0)
;; (setq alu-stat0 (make-array 16))

;; (defvar alu-stat1)
;; (setq alu-stat1 (make-array 16))

;; (defvar alu-stat2)
;; (setq alu-stat2 (make-array 16))

;; (defvar prom-ucode)
;; (setq prom-ucode (make-array 512))

;; (defvar ucode)
;; (setq ucode (make-array '(16 1024)))

;; (defvar a-memory)
;; (setq a-memory (make-array 1024))

;; (defvar m-memory)
;; (setq m-memory (make-array 32))

;; (defvar dispatch-memory)
;; (setq dispatch-memory (make-array 2048))

;; (defvar pdl-memory)
;; (setq pdl-memory (make-array 1024))

;; ;;; 
;; ;; struct page_s {
;; ;; 	unsigned int w[256];
;; ;; };

;; ;; static struct page_s *phy_pages[16*1024];

;; (defvar phy-pages)
;; (setq phy-pages (make-array '(16 1024 256)))

;; (defvar l1-map)
;; (setq l1-map (make-array 2048))

;; (defvar l2-map)
;; (setq l2-map (make-array 1024))

;; (defvar spc-stack)
;; (setq spc-stack (make-array 32))


;; (defun *find_function_name (the_lc)
;;   (error "Not implemented yet!")
;; )

;; (defun video_read (offset *pv)
;;   (error "Not implemented yet!")
;; )

;; (defun video_write (offset bits)
;;   (error "Not implemented yet!")
;; )

;; (defun iob_unibus_read (offset *pv)
;;   (error "Not implemented yet!")
;; )

;; (defun iob_unibus_write (offset v)
;;   (error "Not implemented yet!")
;; )

;; (defun disk_xbus_read (offset *pv)
;;   (error "Not implemented yet!")
;; )

;; (defun disk_xbus_write (offset v)
;;   (error "Not implemented yet!")
;; )

;; (defun tv_xbus_read (offset *pv)
;;   (error "Not implemented yet!")
;; )

;; (defun tv_xbus_write (offset v)
;;   (error "Not implemented yet!")
;; )

;; (defun disassemble_ucode_loc (loc  u)
;;   (error "Not implemented yet!")
;; )

;; (defun sym_find (mcr *name *pval)
;;   (error "Not implemented yet!")
;; )

;; (defun reset_pc_histogram ()
;;   (error "Not implemented yet!")
;; )

;; (defun timing_start ()
;;   (error "Not implemented yet!")
;; )

;; (defun timing_stop ()
;;   (error "Not implemented yet!")
;; )

;; (defun iob_poll ()
;;   (error "Not implemented yet!")
;; )

;; (defun disk_poll ()
;;   (error "Not implemented yet!")
;; )

;; (defun display_poll ()
;;   (error "Not implemented yet!")
;; )

;; (defun chaos_poll ()
;; (error "Not implemented yet!")
;; )

(defvar u_pc)
(defvar interrupt_status_reg)

(defmacro get-u-pc (u_pc)
  u_pc)

(defun parse-string-as-int-field (oct-string &optional (radix 8))
  (let ((integer (parse-integer oct-string :radix radix)))
    (values integer 
            (coerce (ceiling (log (expt 8 (length oct-string)) 2)) 'integer))))
;; bit 'and' useful for porting c
;;
;; if correspending bits of the to two parameters are the same, then
;; the result bit array will be set on for that bit. Otherwise it's
;; set to 0.  The two representations of a bit field are '(<integer>
;; <width>) or <octal-text>.
;; (defun & (bit-field1 bit-field2)
;;   (cond ((and (stringp bit-field1) (listp bit-field2))
;;          (let ((field1-val (parse-string-as-int-field bit-field1))
;;                (field1-width (ceiling (log (expt 8 (length bit-field1)) 2)))
;;                (field2-val (car bit-field2))
;;                (field2-width (cadr bit-field2)))
;;            (cond ((eql field2-width field1-width)
;;                   (logand field1-val
;;                    (ldb (byte (cadr bit-field2) 0)
;;                         (car bit-field2))))
;;                  (((error "Field widths must be the same."))))))
;;         ((error "The combination of datatypes you entered is not supported"))))


                                        ; (& "0140000" '(21 3))

                                        ;(defvar interrupt-pending-flag (u_pc)
                                        ;  (interrupt_status_reg & 0140000) ? 1 : 0;)


;; (defun set_interrupt_status_reg (new);
;; 	(setq interrupt_status_reg new)
;; 	(setq interrupt_pending_flag  (cond ((eql (& "0140000" interrupt_status_reg) 0)
;;                                        0)
;;                                        ((1)))))


(defun process-arg ()
  (list
   '("a" (enable-flag 'alt-prom-flag))
   '("b" (breakpoint-set-mcr optarg))
   '("B" (set-c-global begin-trace-cycle (atol optarg)))
   '("c" (set-c-global max-cycles (atol optarg)))
   '("C" (set-c-global max-trace-cycles (atol optarg)))
   '("d" (enable-flag 'dump-state-flag))
   '("i" (config-set-disk-filename optarg))
   '("l" (tracelabel-set-mcr optarg))
   '("n" (disable-flag 'show-video-flag))
   '("m" (disable-flag 'mouse-sync-flag))
   '("p" (breakpoint-set-prom optarg))
   '("q" (breakpoint-set-count (atoi optarg)))
   '("r" (mam-sit-tree-directory))
   '("S" (enable-flag 'save-state-flag))
   '("t" (enable-flag 'trace))
   '("ta" (enable-flag 'trace-after-flag))
   '("td" (enable-flag 'trace-disk-flag))
   '("ti" (enable-flag 'trace-int-flag))
   '("to" (enable-flag 'trace-io-flag))
   '("tp" (enable-flag 'trace-prom-flag))
   '("tc" (enable-flag 'trace-mcr-flag))
   '("tm" (enable-flag 'trace-mcr-labels-flag))
   '("tn" (enable-flag 'trace-net-flag))
   '("tl" (enable-flag 'trace-lod-labels-flag))
   '("tv" (enable-flag 'trace-vm-flag))
   '("s" (enable-flag 'stop-after-prom-flag))
   '("w" (enable-flag 'warm-boot-flag))
   ))

(defun pre-lisp-main ()
  (print "CADR emulator v0.9")
  (display-init)
  (display-poll)
  (disk-init
   (config-get-disk-filename))

  (read-prom-files)
  (read-sym-files)
  (iob-init)
  (chaos-init)
  (ether-init)
  (show-prom)
  (disassemble-prom)
  (signal-init)
  (cond ((/= (get-warm-boot-flag) 0)
         (iob-warm-boot-key)))
  (lisp-run)
  (lisp-main)  
  )

;; for now
(let ((pack (find-package :demo)))
  (do-all-symbols (sym pack) (when (eql (symbol-package sym) pack) (export sym))))


;;(load "ucode.lisp")
;;(demo:pre-lisp-main)
