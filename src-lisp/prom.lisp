;;; -*- Mode:LISP; Syntax:Common-lisp; Package:USER; Base: 10; Lowercase: Yes -*-

;; int32_t MemSpaces::read_prom_files() {
;;   int fd, i;
;;   ssize_t ret;

;; ;;  for (i = 0; i < 6; i++) {

;; (loop for i from 0 to 5
;;      do

;;     char name[256];

;;     sprintf(name, "cadr_%1d.bin", i+1);
;;     fd = open(name, O_RDONLY | O_BINARY);



(defun files-are-same-size (file-names) 
  (apply
   '=
   (mapcar 
    (lambda (file-name)
      (with-open-file 
	  (s file-name :element-type 'unsigned-byte)
	(setq file-size (file-length s))))
    file-names))
  )

(defun files-are-same-size (file-names) 
  (apply
   '=
   (mapcar 
    (lambda (file-name)
      (with-open-file 
	  (s file-name :element-type 'unsigned-byte)
	(setq file-size (file-length s))))
    file-names))
  )

(defun get-file-size (file-name)
  (with-open-file (s file-name :element-type 'unsigned-byte)
    (file-length s)))

(defun get-prom ()
  (let ((file-names (list "cadr_1.bin" 
			  "cadr_2.bin" 
			  "cadr_3.bin" 
			  "cadr_4.bin" 
			  "cadr_5.bin"
			  "cadr_6.bin")))
    (if (files-are-same-size file-names)
	;; since all files are the same size...
	(let ((file-size (get-file-size (first file-names))))
	  ;; make sure all files have the same number of bytes.
	  (let ((prom-mem (make-array (* file-size (length file-names))
				      :initial-element 0)))
	    (loop for file-name in file-names do 
		 (loop for i from 0 to (1- (length file-names)) do
		      (with-open-file 
			  (s (elt file-names i) :element-type 'unsigned-byte)
			(loop for j from 0 to (1- file-size) do
			     (setf
			      (aref prom-mem (+ (* j (length file-names)) i))
			      (read-byte s))
			     ))))
	    prom-mem)))))

  ;; (let ((prom-size nil)
  ;; 	(byte-list
  ;; 	 (reverse
  ;; 	  (with-open-file (s file-name :element-type 'unsigned-byte)
  ;; 	    (cond (((not prom-size)
  ;; 		    (setq prom-size (file-length s)))
  ;; 		   ((= prom-size
  ;; 		       (file-length s))
  ;; 		    (let ((byte nil)
  ;; 			  (bytes '()))
  ;; 	      (loop while (not (eq (setq byte (read-byte s nil 'eof)) 'eof))
  ;; 		 do
  ;; 		   (push byte bytes))
  ;; 	      bytes)))))
  ;;   (make-array (length byte-list) :initial-contents byte-list)))

  ;; (defun get-prom (file-name)
  ;;   (let ((byte-list
  ;; 	 (reverse
  ;; 	  (with-open-file (s file-name :element-type 'unsigned-byte)
  ;; 	    (let ((byte nil)
  ;; 		  (bytes '()))
  ;; 	      (loop while (not (eq (setq byte (read-byte s nil 'eof)) 'eof))
  ;; 		 do
  ;; 		   (push byte bytes))
  ;; 	      bytes)))))
  ;;     (make-array (length byte-list) :initial-contents byte-list)))






  ;;        (prom-banks
  ;; 	(mapcar 'get-prom prom-files))
  ;;        (if (validate-proms)	   
  ;; 	   (loop for i from 0 to (- (length (aref prom-banks 0)) 1) do
  ;; 		(prom_memory
  ;; 		 (loop for prom-bank across prom-banks do 
  ;; 		      (push (aref prom-bank i)
  ;; 		 (mapc get-prom 





  ;;     if (fd < 0) {
  ;;       perror(name);
  ;;       exit(1);
  ;;     }

  ;;     ret = read(fd, prom[i], 512);
  ;;     close(fd);

  ;;     if (ret != 512) {
  ;;       fprintf(stderr, "read_prom_files: short read\n");
  ;;       exit(1);
  ;;     }
  ;;   }

  ;;   for (i = 0; i < 512; i++) {
  ;;     prom_ucode[511-i] =
  ;;       ((uint64)prom[0][i] << (8*5)) |
  ;;       ((uint64)prom[1][i] << (8*4)) |
  ;;       ((uint64)prom[2][i] << (8*3)) |
  ;;       ((uint64)prom[3][i] << (8*2)) |
  ;;       ((uint64)prom[4][i] << (8*1)) |
  ;;       ((uint64)prom[5][i] << (8*0));
  ;;   }

  ;;   return 0;
  ;; }


;;  (defun write-null-terminated-ascii (string out)
;;    (loop for char across string
;;       do (write-byte (char-code char) out))
;;    (write-byte (char-code +null+) out))

;;;  (defun read-null-terminated-ascii (in)
;;;    (with-output-to-string (s)
;;;     (loop for char = (code-char (read-byte in))
;;;	 until (char= char +null+) do (write-char char s))))
