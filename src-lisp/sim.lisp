;;(in-package #:sim)

;;; The machine instruction data structure simply pairs the
;;; instruction text with the corresponding execution procedure. The
;;; execution procedure is not yet available when extract-labels
;;; constructs the instruction, and is inserted later by
;;; update-insts!.

(defun make-instruction (text)
  (cons text '()))

(defun decode-instruction (instr)		      ;
  (cons instr '()))

(defun instruction-text (inst)
  (car inst))
(defun instruction-execution-proc (inst)
  (cdr inst))
(defun set-instruction-execution-proc! (inst proc)
  (setf (cdr inst) proc))

;;; The instruction text is not used by our simulator, but it is handy
;;; to keep around for debugging (see exercise 5.16).

;;; Elements of the label table are pairs:

(defun make-label-entry (label-name insts)
  (cons label-name insts))

;;; Extract-labels takes as arguments a list text (the sequence of
;;; controller instruction expressions) and a receive
;;; procedure. Receive will be called with two values: (1) a list
;;; insts of instruction data structures, each containing an
;;; instruction from text; and (2) a table called labels, which
;;; associates each label from text with the position in the list
;;; insts that the label designates.

(defun extract-labels (text receive)
  (if (null text)
      (funcall receive '() '())
      (extract-labels (cdr text)
		      (lambda (insts labels)
			(let ((next-inst (car text)))
			  (if (symbolp next-inst)
			      (funcall receive insts
				       (cons (make-label-entry next-inst
							       insts)
					     labels))
			      (funcall receive (cons (make-instruction next-inst)
						     insts)
				       labels)))))))




(defun extract-bin-labels (bin-instructions receive)
  (if (= (length  bin-instructions) 0)
      (funcall receive '() '())
      (let ((instruction (elt bin-instructions 0)))
	(setq bin-instructions (subseq bin-instructions 1))
	(extract-bin-labels
	 bin-instructions
	 (lambda (insts labels)
	   (if (symbolp instruction)
	       (funcall receive insts
			(cons (make-label-entry instruction
						insts)
			      labels))
	       (funcall receive (cons (decode-instruction instruction)
				      insts)
			labels)))))))

(defun  tagged-list? (exp tag)
  (if (listp exp)
      (eq (car exp) tag)
      false))

;;; The syntax of operation expressions is determined by

(defun operation-exp? (exp)
  (and (listp exp) (tagged-list? (car exp) 'op)))
(defun operation-exp-op (operation-exp)
  (cadr (car operation-exp)))
(defun operation-exp-operands (operation-exp)
  (cdr operation-exp))

;;; Assign, perform, and test instructions may include the application
;;; of a machine operation (specified by an op expression) to some
;;; operands (specified by reg and const expressions). The following
;;; procedure produces an execution procedure for an ``operation
;;; expression'' -- a list containing the operation and operand
;;; expressions from the instruction:

(defun make-operation-exp (exp machine labels operations)
  (let ((op (lookup-prim (operation-exp-op exp) operations))
        (aprocs
         (mapcar (lambda (e)
		   (make-primitive-exp e machine labels))
		 (operation-exp-operands exp))))
    (lambda ()
      (apply op (mapcar (lambda (p) (funcall p)) aprocs)))))

;;; Advance-pc is the normal termination for all instructions except
;;; branch and goto.

;;; Test, branch, and goto instructions

;;; Make-test handles test instructions in a similar way. It extracts
;;; the expression that specifies the condition to be tested and
;;; generates an execution procedure for it. At simulation time, the
;;; procedure for the condition is called, the result is assigned to
;;; the flag register, and the pc is advanced:

(defun make-test (inst machine labels operations flag pc)
  (let ((condition (test-condition inst)))
    (if (operation-exp? condition)
        (let ((condition-proc
               (make-operation-exp
                condition machine labels operations)))
          (lambda ()
            (set-contents! flag (funcall condition-proc))
            (advance-pc pc)))
        (error "Bad TEST instruction -- ASSEMBLE"))))

(defun test-condition (test-instruction)
  (cdr test-instruction))

(defun branch-dest (branch-instruction)
  (cadr branch-instruction))

;;; Make-assign extracts the target register name (the second element
;;; of the instruction) and the value expression (the rest of the list
;;; that forms the instruction) from the assign instruction using the
;;; selectors

(defun assign-reg-name (assign-instruction)
  (cadr assign-instruction))
(defun assign-value-exp (assign-instruction)
  (cddr assign-instruction))

;;; For each type of instruction in the register-machine language,
;;; there is a generator that builds an appropriate execution
;;; procedure. The details of these procedures determine both the
;;; syntax and meaning of the individual instructions in the
;;; register-machine language. We use data abstraction to isolate the
;;; detailed syntax of register-machine expressions from the general
;;; execution mechanism, as we did for evaluators in section 4.1.2, by
;;; using syntax procedures to extract and classify the parts of an
;;; instruction.

;;; Assign instructions

;;; The make-assign procedure handles assign instructions:

(defun make-assign (inst machine labels operations pc)
  (let ((target
         (get-register machine (assign-reg-name inst)))
        (value-exp (assign-value-exp inst)))
    (let ((value-proc
           (if (operation-exp? value-exp)
               (make-operation-exp
                value-exp machine labels operations)
               (make-primitive-exp
                (car value-exp) machine labels))))
      (lambda ()                ; execution procedure for assign
        (set-contents! target (funcall value-proc))
        (advance-pc pc)))))

(defun goto-dest (goto-instruction)
  (cadr goto-instruction))

(defun make-bin1 (inst machine labels operations pc)
  (lambda ()                ; execution procedure for assign
    (format t "xbus io     ~32,'0b~%" inst)
    (advance-pc pc)))

(defun make-bin0 (inst machine labels operations pc)
  (lambda ()                ; execution procedure for assign
    (print "bin0")
    (advance-pc pc)))

;;; A goto instruction is similar to a branch, except that the
;;; destination may be specified either as a label or as a register,
;;; and there is no condition to check -- the pc is always set to the
;;; new destination.

(defun make-goto (inst machine labels pc)
  (let ((dest (goto-dest inst)))
    (cond ((label-exp? dest)
           (let ((insts
                  (lookup-label labels
                                (label-exp-label dest))))
             (lambda () (set-contents! pc insts))))
          ((register-exp? dest)
           (let ((reg
                  (get-register machine
                                (register-exp-reg dest))))
             (lambda ()
               (set-contents! pc (get-contents reg)))))
          (t (error "Bad GOTO instruction -- ASSEMBLE")))))

;;; With the simulator as written, what will the contents of register
;;; a be when control reaches there? Modify the extract-labels
;;; procedure so that the assembler will signal an error if the same
;;; label name is used to indicate two different locations.

;;; 5.2.3  Generating Execution Procedures for Instructions

;;; The assembler calls make-execution-procedure to generate the
;;; execution procedure for an instruction. Like the analyze procedure
;;; in the evaluator of section 4.1.7, this dispatches on the type of
;;; instruction to generate the appropriate execution procedure.

(defun make-execution-procedure (inst labels machine
				 pc flag stack ops)
  (cond ((eq (car inst) 'assign)
         (make-assign inst machine labels ops pc))
        ((eq (car inst) 'test)
         (make-test inst machine labels ops flag pc))
        ((eq (car inst) 'branch)
         (make-branch inst machine labels flag pc))
        ((eq (car inst) 'goto)
         (make-goto inst machine labels pc))
        ((eq (car inst) 'save)
         (make-save inst machine stack pc))
        ((eq (car inst) 'restore)
         (make-restore inst machine stack pc))
        ((eq (car inst) 'perform)
         (make-perform inst machine labels ops pc))
        (t (error "Unknown instruction type -- ASSEMBLE"))))

(defun make-bin-execution-procedure (inst labels machine
				     pc flag stack ops)
  (cond (t
         (make-bin1 inst machine labels ops pc))
        (t (make-bin0 inst machine labels ops pc))))



;;; Update-insts! modifies the instruction list, which initially
;;; contains only the text of the instructions, to include the
;;; corresponding execution procedures:

(defun update-insts! (insts labels machine)
  (let ((pc (get-register machine 'pc))
        (flag (get-register machine 'flag))
        (stack (funcall machine 'stack))
        (ops (funcall machine 'operations)))
    (mapcar
     (lambda (inst)
       (set-instruction-execution-proc!
        inst
        (make-bin-execution-procedure
         (instruction-text inst) labels machine
         pc flag stack ops)))
     insts)))

(defun update-bin-insts! (insts labels machine)
  (let ((pc (get-register machine 'pc))
        (flag (get-register machine 'flag))
        (stack (funcall machine 'stack))
        (ops (funcall machine 'operations)))
    (mapcar
     (lambda (inst)
       (set-instruction-execution-proc!
        inst
        (make-bin-execution-procedure
         (instruction-text inst) labels machine
         pc flag stack ops)))
     insts)))

;;; Observe that the treatment of operation expressions is very much
;;; like the treatment of procedure applications by the
;;; analyze-application procedure in the evaluator of section 4.1.7 in
;;; that we generate an execution procedure for each operand. At
;;; simulation time, we call the operand procedures and apply the
;;; Scheme procedure that simulates the operation to the resulting
;;; values. The simulation procedure is found by looking up the
;;; operation name in the operation table for the machine:

(defun lookup-prim (symbol operations)
  (let ((val (assoc symbol operations)))
    (if val
        (cadr val)
        (error "Unknown operation -- ASSEMBLE"))))

;;; The syntax of reg, label, and const expressions is determined by

(defun register-exp? (exp) (tagged-list? exp 'reg))
(defun register-exp-reg (exp) (cadr exp))
(defun constant-exp? (exp) (tagged-list? exp 'const))
(defun constant-exp-value (exp) (cadr exp))
(defun label-exp? (exp) (tagged-list? exp 'label))
(defun label-exp-label (exp) (cadr exp))

;;; Extract-labels works by sequentially scanning the elements of the
;;; text and accumulating the insts and the labels. If an element is a
;;; symbol (and thus a label) an appropriate entry is added to the
;;; labels table. Otherwise the element is accumulated onto the insts
;;; list.4


;;; Entries will be looked up in the table with

(defun lookup-label (labels label-name)
  (let ((val (assoc label-name labels)))
    (if val
        (cdr val)
        (error "Undefined label -- ASSEMBLE"))))

;;; The execution procedure for a branch instruction checks the
;;; contents of the flag register and either sets the contents of the
;;; pc to the branch destination (if the branch is taken) or else just
;;; advances the pc (if the branch is not taken). Notice that the
;;; indicated destination in a branch instruction must be a label, and
;;; the make-branch procedure enforces this. Notice also that the
;;; label is looked up at assembly time, not each time the branch
;;; instruction is simulated.

(defun make-branch (inst machine labels flag pc)
  (let ((dest (branch-dest inst)))
    (if (label-exp? dest)
        (let ((insts
               (lookup-label labels (label-exp-label dest))))
          (lambda ()
            (if (get-contents flag)
                (set-contents! pc insts)
                (advance-pc pc))))
        (error "Bad BRANCH instruction -- ASSEMBLE"))))

;;; Execution procedures for subexpressions

;;; The value of a reg, label, or const expression may be needed for
;;; assignment to a register (make-assign) or for input to an
;;; operation (make-operation-exp, below). The following procedure
;;; generates execution procedures to produce values for these
;;; expressions during the simulation:


(defun make-primitive-exp (exp machine labels)
  (cond ((constant-exp? exp)
         (let ((c (constant-exp-value exp)))
           (lambda () c)))
        ((label-exp? exp)
         (let ((insts
                (lookup-label labels
                              (label-exp-label exp))))
           (lambda () insts)))
        ((register-exp? exp)
         (let ((r (get-register machine
                                (register-exp-reg exp))))
           (lambda () (get-contents r))))
        (t
         (error "Unknown expression type -- ASSEMBLE"))))

;;;-------------------------------------------------------------


(defun make-register (name)
  (let ((contents '*unassigned*))
    (labels ((dispatch (message)
	       (cond ((eq message 'get) contents)
		     ((eq message 'set)
		      (lambda (value) (setq contents value)))
		     (t
		      (error "Unknown request -- REGISTER")))))
      (lambda (message) (dispatch message)))))

(defun get-contents (register)
  (funcall register 'get))

(defun set-contents! (register value)
  (funcall (funcall register 'set) value))

(defun make-stack ()
  (let ((s '())
	(number-pushes 0)
	(max-depth 0)
	(current-depth 0))
    (labels ((my-push (x)
	       (setq s (cons x s))
	       (setq number-pushes (+ 1 number-pushes))
	       (setq current-depth (+ 1 current-depth))
	       (setq max-depth (max current-depth max-depth))
	       )
             (my-pop ()
	       (if (null s)
		   (error "Empty stack -- MY-POP")
		   (let ((top (car s)))
		     (setq s (cdr s))
		     (setq current-depth (- current-depth 1))top)))
             (initialize ()
	       (setq s '())
	       (setq number-pushes 0)
	       (setq max-depth 0)
	       (setq current-depth 0)
	       'done)
             (print-statistics()
	       (princ (list 'total-pushes  '= number-pushes
			    'maximum-depth '= max-depth)))
	     (dispatch (message)
	       (cond ((eq message 'my-push) (lambda (val) (my-push val)))
		     ((eq message 'my-pop) (my-pop))
		     ((eq message 'print-statistics) (print-statistics))
		     ((eq message 'initialize) (initialize))
		     (t (error "Unknown request -- STACK")))))
      (lambda (message) (dispatch message)))))

(defun my-pop (stack)
  (funcall stack 'my-pop))

(defun my-push (stack value)
  (funcall (funcall stack 'my-push) value))


(defclass machine ()
  ((the-machine :accessor the-machine :initform '())
  (program-counter :initform (make-register 'pc))
   (the-instruction-sequence :initform '())))

(defmethod initialize ((m machine) registers buses memory-areas instr-mode instr)
  (let ((machine (make-new-machine)))
    (mapcan (lambda (register)
              (funcall (funcall machine 'allocate-register) register))
            registers)

    (funcall (funcall machine 'install-instruction-mode) instr-mode)
    (set-contents! (slot-value m 'program-counter) 0)
    (cond ((eq instr-mode 'symbolic)
    	   (setf (slot-value m 'the-instruction-sequence)
		 (assemble controller-text machine)))
    	  ((eq instr-mode 'binary)
    	   ;; (funcall (funcall machine 'install-instruction-sequence)
    	   ;; 	    (bin-assemble bin-instructions machine)))

	   (setf (slot-value m 'the-instruction-sequence) instr))


    	  (t
    	   (error (concatenate "Unknown instr-mode: " (string instr-mode)))))
    (setf (the-machine m) machine)
    machine))

;; Both our data sources are enumerated in bytes which
;; we convert into LM words of 48 bits.
;;
;; TODO (eek) -- Sometime we should convert these to
;; streams of the appropriate size.
(defmethod peek-next-instruction ((m machine))
  (let* ((pc-val (get-contents (slot-value m 'program-counter)))
	 (the-instruction-sequence (slot-value m 'the-instruction-sequence))
	 (from-pc (* pc-val 6))
	 (to-pc (+ (* pc-val 6) 6)))
    (subseq
     the-instruction-sequence
     from-pc
     to-pc)))

(defmethod read-next-instruction ((m machine))
  (let ((instr (peek-next-instruction m))
	(program-counter (get-contents (slot-value m 'program-counter))))
    (if (not (null instr))
	(setf program-counter (1+ program-counter)))
    instr
    program-counter
    ))


;; (defun cycle (machine times)
;;   (funcall (funcall machine 'cycle) 1))

(defgeneric cyle (m times))

(defmethod cycle ((m machine) times)
  (let ((inst (slot-value m 'the-instruction-sequence)))
    (if (or (null insts)
	    (eq insts 0)
	    (eq times 0))
	'done
	(if (null the-instruction-sequence)
	    (progn
	      ;; We must be processing binary instructions.
	      (funcall (instruction-execution-proc (car insts)))
	      (cycle (1- times)))
	    (progn
	      ;; We must be processing symbolic instructions.
	      (funcall (instruction-execution-proc (car insts)))
	      (cycle (1- times)))
	    ))))


(defvar *the-machine* nil)

;; (defmethod set-machine ((m machine) val)
;;   (setq (the-machine m)  val))

(defun make-new-machine ()
  (let ((pc (make-register 'pc))
        (flag (make-register 'flag))
        (stack (make-stack))
	(instruction-mode 'symbolic))    ;; stream of instructions
    (let ((the-ops
           (list (list 'initialize-stack
                       (lambda () (funcall stack 'initialize)))))
          (register-table
           (list ;;(list 'program-counter program-counter)
		 (list 'pc pc)
		 (list 'flag flag)))
	  )
	;; a dispatch
        (lambda (message)
          (cond ((eq message 'cycle)
                 (lambda (times)
		   (if (not (null the-bin-instruction-sequence))
		       (set-contents! pc the-bin-instruction-sequence)
		       (set-contents! pc the-instruction-sequence))
		   (cycle times)
		   ))
		((eq message 'start)
                 (set-contents! pc the-instruction-sequence)
                 (cycle 10000))
                ;; ((eq message 'install-instruction-sequence)
                ;;  (lambda (seq) (setq the-instruction-sequence seq)))
                ((eq message 'install-instruction-mode)
                 (lambda (mode) (setq instruction-mode mode)))
                ((eq message 'allocate-register)
                 (lambda (name)
                   (if (assoc name register-table)
                       (error (concatenate
			       'string
			       "Multiply defined register: "
			       (string name)))
		       (setq register-table
			     (cons (list name (make-register name))
				   register-table)))
                   'register-allocated))
                ((eq message 'get-register)
                 (lambda (name)
                   (let ((val (assoc name register-table)))
                     (if val
                         (cadr val)
			 (error (concatenate
				 'string
				 "Unknown register: "
				 (string name)))))))
                ((eq message 'install-operations)
                 (lambda (ops) (setq the-ops (append the-ops ops))))
                ((eq message 'stack) stack)
                ((eq message 'operations) the-ops)
                (t (error (concatenate
			   'string
			   "Unknown machine message: "
			   (string message)))))))))
(defun start (machine)
  (funcall machine 'start))


(defun get-register-contents (machine register-name)
  (get-contents (get-register machine register-name)))

(defun get-register (machine reg-name)
  (funcall (funcall machine 'get-register) reg-name))

(defun set-register-contents! (machine register-name value)
  (set-contents! (get-register machine register-name) value)
  'done)

(defun remainder (x y)
  (mod x y))

(defun assemble (controller-text machine)
  (extract-labels controller-text
		  (lambda (insts labels)
		    (update-insts! insts labels machine)
		    insts)))

(defun bin-assemble (bin-instructions machine)
  (extract-bin-labels bin-instructions
		      (lambda (insts labels)
			(update-bin-insts! insts labels machine)
			insts)))

;;; The register name is looked up with get-register to produce the
;;; target register object. The value expression is passed to
;;; make-operation-exp if the value is the result of an operation, and
;;; to make-primitive-exp otherwise. These procedures (shown below)
;;; parse the value expression and produce an execution procedure for
;;; the value. This is a procedure of no arguments, called value-proc,
;;; which will be evaluated during the simulation to produce the
;;; actual value to be assigned to the register. Notice that the work
;;; of looking up the register name and parsing the value expression
;;; is performed just once, at assembly time, not every time the
;;; instruction is simulated. This saving of work is the reason we use
;;; execution procedures, and corresponds directly to the saving in
;;; work we obtained by separating program analysis from execution in
;;; the evaluator of section 4.1.7.

;;; The result returned by make-assign is the execution procedure for
;;; the assign instruction. When this procedure is called (by the
;;; machine model's cycle procedure), it sets the contents of the
;;; target register to the result obtained by executing
;;; value-proc. Then it advances the pc to the next instruction by
;;; running the procedure

(defun advance-pc (pc)
  (set-contents! pc (cdr (get-contents pc))))

;;; Other instructions

;;; The stack instructions save and restore simply use the stack with
;;; the designated register and advance the pc:

(defun make-save (inst machine stack pc)
  (let ((reg (get-register machine
                           (stack-inst-reg-name inst))))
    (lambda ()
      (push stack (get-contents reg))
      (advance-pc pc))))

(defun make-restore (inst machine stack pc)
  (let ((reg (get-register machine
                           (stack-inst-reg-name inst))))
    (lambda ()
      (set-contents! reg (pop stack))
      (advance-pc pc))))

(defun stack-inst-reg-name (stack-instruction)
  (cadr stack-instruction))

;;; The final instruction type, handled by make-perform, generates an
;;; execution procedure for the action to be performed. At simulation
;;; time, the action procedure is cycled and the pc advanced.

(defun make-perform (inst machine labels operations pc)
  (let ((action (perform-action inst)))
    (if (operation-exp? action)
        (let ((action-proc
               (make-operation-exp
                action machine labels operations)))
          (lambda ()
            (action-proc)
            (advance-pc pc)))
        (error "Bad PERFORM instruction -- ASSEMBLE"))))

(defun perform-action (inst) (cdr inst))


;;(defun get-register (machine reg-name)
;;  (funcall (funcall machine 'get-register) reg-name))
