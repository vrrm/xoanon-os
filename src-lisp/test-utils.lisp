(defun run-tests (tests) 
  (mapcar (lambda (x) 
            (cons 
             (car x) 
             (funcall (caddr x) (list '()))))
          tests))

;;; tests should look like this:
;;; (<id> . (lambda (<env>) ...))
;;;      where env is a list of definitions needed to run 

(defvar tests)
(setq tests
      (list 
       (list 2
             "Test setup environment"
             (lambda (env)
               (if (eq (apply-primitive-procedure 
                        (list 'my-car 
                              (lambda (x) (car x)))
                        (list '(1 2)))
                       1)
                   t 
                 '()))
             '(APPLY-PRIMITIVE-PROCEDURE PRIMITIVE-PROCEDURE)
             )
       (list 3
             "Test setup environment"
             (lambda (env)
               (if (assignmentp (list 'set! 'a 1))
                   t
                 '()))
             '(ASSIGNMENTP)
             )
       (list 4
             "Test self-evaluatingp"
             (lambda (env)
               (if (and 
                    (not (self-evaluatingp 'a))
                    (self-evaluatingp 1)
                    (self-evaluatingp "a"))
                   t
                 '()))
             '(SELF-EVALUATINGP)
             )
       (list 5
             ""
             (lambda (env)
               ;; (if (and 
               ;;      (not (self-evaluatingp 'a))
               ;;      (self-evaluatingp 1)
               ;;      (self-evaluatingp "a"))
               ;;     t
               ;;   '())
               )
             '(SELF-EVALUATINGP)
             )
       ))

(defun file-string (path)
  "Sucks up an entire file from PATH into a freshly-allocated string,
      returning two values: the string and the number of bytes read."
  (with-open-file (s path)
                  (let* ((len (file-length s))
                         (data (make-string len)))
                    (values data (read-sequence data s)))))



(defun get-code-file ()
  (cadr(read-from-string (concatenate 'string  " '(" 
                                      (file-string "lisp-on-lisp-sicp.lisp")
                                      ")"))))

(defun file-forms (path)
  "Sucks up an entire file from PATH into a list of forms (sexprs),
      returning two values: the list of forms and the number of forms read."
  (with-open-file (s path)
                  (loop with eof = (list nil)
                        for form = (read s nil eof)
                        until (eq form eof)
                        collect form into forms
                        counting t into form-count
                        finally (return (values forms form-count)))))

(defun get-functions () 
  (remove nil (mapcar
               (lambda (x) 
                 (if (listp x)
                     (cadr x)))
               (get-code-file)) 
          ))


(defun get-functions-without-tests () 
  (let ((functions-without-tests '()))
    (maphash 
     (lambda (key value) 
       (push key functions-without-tests))
     (let ((hash-table (make-hash-table :test 'equal)))
       (mapcar
        (lambda (x)
          (setf (gethash x hash-table) x))
        (get-functions))
       (mapcar
        (lambda(test)
          (mapcar
           (lambda (function-name)
             (remhash function-name hash-table))
           (cadddr test)))
        tests)
       hash-table))
    functions-without-tests
    ))

