(sb-ext:restrict-compiler-policy 'debug 3)
;;(load "instruction-stream.lisp")

(defvar registers
  '(
    a
    b
    t
    spc-ptr
    ))

(defvar buses
  '(xbus))

(defvar memory-areas
  '((xbus a 1024)
    (xbus m 1024)
    (xbus prom 1024)
    (xbus pdl 1024*32)
    (xbus spc 32*15)
    (xbus im 4096*42)))


(defvar bin-instructions nil)


(defun allocate-bin-instructions ()
  (get-prom))

(defvar instructions
   '(test-b
     (test (op =) (reg b) (const 0))
     (branch (label gcd-done))
     (assign t (op rem) (reg a) (reg b))
     (assign a (reg b))
     (assign b (reg t))
     (goto (label test-b))
     gcd-done))


(defvar ops
   (list
    (list 'rem
          (lambda (x y)
            (remainder x y)))
    (list '=
          (lambda (x y)
            (= x y)))))

;; (defun run-my-cadr ()
;;   (let ((my-cadr
;; 	  (make-machine
;; 	    registers
;; 	    buses
;; 	    memory-areas
;; 	    'binary
;; 	    (make-array 1024 :element-type 'unsigned-byte))))

;;     my-cadr
;;     ;;(start my-cadr)
;;     ;;(cycle my-cadr 1)
;;     ))


(defvar my-machine (make-instance 'machine))

(defun initialize-test-machine ()
  (initialize my-machine
	      registers
	      buses
	      memory-areas
	      'binary
	      bin-instructions))
