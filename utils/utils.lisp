;;; -*- Mode:LISP; Syntax:Common-lisp; Package:xoanon-utils; Base: 10; Lowercase: Yes -*-

(in-package :xoanon-utils)

(defun make-pathname-child-folder (pathname child-name)
  (make-pathname
   :directory
   (concatenate 'list (pathname-directory pathname) (list child-name))))

(defun make-pathname-child-file (pathname child-name)
  (make-pathname
   :name
    child-name
    :directory
    (pathname-directory pathname)))

(defvar *default-project-directory*
   (asdf:system-source-directory :xoanon))

(defvar *default-tftp-directory*
  (make-pathname-child-folder  *default-project-directory* "tftp"))

(defvar *default-src-c-directory*
  (make-pathname-child-folder *default-project-directory* "src-c"))

(defun str (&rest strings)
  (apply #'concatenate 'string strings))

(defun build-tftp ()
  (run/lines (list 'mkdir "-p" (str (namestring *default-tftp-directory*) "boot/grub") )))

;  (run/lines '(qemu-system-i386 "-boot" "n" "-net" "nic,model=ne2k_pci" "-net" "user,tftp=/home/vrrm/dev/xinu/xinu-vm/xinu-vm/tftp/boot/grub,bootfile=i386-pc/core.0")))

;; (defun build-tftp-directory ()
;;   (run/lines `(rm -rf ,*default-project-directory*)))

(defun build-kernel ()
  (let ((compile-dir (namestring (make-pathname-child-folder *default-src-c-directory* "compile"))))
    (run/lines `(make "-C" ,compile-dir clean))
    (run/lines `(make "-C" ,compile-dir))))

(defun install-kernel ()
  (let* ((from-file (make-pathname-child-file (make-pathname-child-folder
					      *default-src-c-directory*
					      "compile") "xinu"))
	(to-folder (make-pathname-child-folder
		    (make-pathname-child-folder
		     *default-tftp-directory*
		     "boot")
		    "grub"))
	(to-file (make-pathname-child-file
		  to-folder "xinu")))
    (run/lines `(rm ,to-file))
    (run/lines `(cp ,from-file ,to-folder))))


(defun grub-mknetdir ()
    (run/lines `(grub-mknetdir
		 "--subdir=/"
		 "--net-directory=/home/vrrm/dev/xoanon/tftp/boot/grub"
		 "--locale-directory=/usr/share/cups/locale")))



;mv xinu boot/grub/
; grub-mknetdir --subdir=/ --net-directory=/home/vrrm/dev/xinu/xinu-vm/xinu-vm/tftp --locale-directory=/usr/share/cups/locale


;; ./configure --without-dffi --without-profile  --without-rt --without-clos-streams --without-clx --without-serve-event --without-tcp --without-__thread --without-system-gmp --disable-threads --disable-shared

(defun run-kernel (&key debug)
  (let ((command (concatenate
		  'list
		  '(qemu-system-i386 "-boot" "n" "-net" "nic,model=ne2k_pci"
		    "-net" "user,tftp=/home/vrrm/dev/xoanon/tftp/boot/grub,bootfile=i386-pc/core.0")
		  (and debug '("-S" "-s")))))
    (run/lines command)))


(defun full-build ()
  (build-kernel)
  (install-kernel)
  (run-kernel))


(defun get-web-page-html ()
  (let ((url "http://ftp.gnu.org/gnu/mpc/"))
    (run/lines `(wget "-S" "-0" ,url))))

(defvar *built-xml*
  (let ((strm (make-in-memory-input-stream
	       (string-to-octets
		"<example><child1><p>foo</p></child1><child2 bar=\"baz\"/></example>"))))
    (cxml:parse-stream strm (cxml-xmls:make-xmls-builder))))
