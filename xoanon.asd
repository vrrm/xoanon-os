;;;; cadr-emulator.asd

(asdf:defsystem #:xoanon
    :serial t
    :description
    "Various tools for for automating Xoanon OS tasks and bootstraping
   development."
    :author "Vr. RM <vrrm00@gmail.com>"
    :license "MIT(ish)"
    :depends-on (:cffi :cl-ppcre :osicat :cl-openstack :flexi-streams :cxml
		       :inferior-shell :bordeaux-threads :cl-fad)
    :components ((:file "packages")
		 (:module utils :serial t
			  :components
			  ((:file "utils")
			   (:file "cadr-utils" :depends-on (utils))))
		 (:module src-lisp :serial t
			  :components
			  ((:file "bit-manipulation")
			   (:file "prom")
			   (:file "sim")
			   (:file "machine-desc" :depends-on ("sim" "prom"))
			   ))))
